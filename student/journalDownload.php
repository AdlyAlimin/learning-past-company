<!DOCTYPE html>
<?php 
session_start();
include("../process/user_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Journal Download</li>
                        </ol>
                    </div>
                </div>
    

                <div class="row">
                <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">Journal List</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example-advance-1" class="display" style="min-width: 845px">
                                    <thead>
                                        <tr>
                                            <th>Journal Title</th>
                                            <th>Category</th>
                                            <th>Upload Date</th>
                                            <th>Owner</th>
                                            <th>Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sql1="SELECT * FROM journal, student WHERE journal.owner_email=student.email";

                                        if ($result1=mysqli_query($con,$sql1))
                                        {
                                        // Fetch one and one row
                                        while ($row1=mysqli_fetch_array($result1))
                                            {
                                    ?>
                                        <tr>
                                            <td><?php echo $row1['journal_title']; ?></td>
                                            <td><?php echo $row1['category']; ?></td>
                                            <td><?php echo $row1['upload_date']; ?></td>
                                            <td><?php echo $row1['full_name']; ?></td>
                                            <td><a href="../process/download?file=<?php echo base64_encode($row1['journal_path']); ?>"><img src="../assets/images/icon/icons8-Download.png" alt=""></a></td>
                                        </tr>
                                    <?php                     
                                            }
                                        }
                                    
                                        $sql2="SELECT * FROM journal, lecture WHERE journal.owner_email=lecture.email";

                                        if ($result2=mysqli_query($con,$sql2))
                                        {
                                        // Fetch one and one row
                                        while ($row2=mysqli_fetch_array($result2))
                                            {
                                        ?>
                                        <tr>
                                            <td><?php echo $row2['journal_title']; ?></td>
                                            <td><?php echo $row2['category']; ?></td>
                                            <td><?php echo $row2['upload_date']; ?></td>
                                            <td><?php echo $row2['full_name']; ?></td>
                                            <td><a href="../process/download?file=<?php echo base64_encode($row2['journal_path']); ?>"><img src="../assets/images/icon/icons8-Download.png" alt=""></a></td>
                                        </tr>
                                    <?php                     
                                            }
                                        // Free result set
                                        mysqli_free_result($result1);
                                        mysqli_free_result($result2);
                                        }
                                    ?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>