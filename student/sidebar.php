<div class="nk-sidebar">           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Dashboard</li>
            <li><a href="index"><i class="mdi mdi-email-outline"></i><span class="nav-text">Homepage </span></a></li>
            
            <!-- <li class="nav-label">Download</li>
            <li><a href="thesisDownload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Thesis</span></a></li>
            <li><a href="journalDownload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Journal</span></a></li> -->
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Download</span></a>
                <ul aria-expanded="false">
                    <li><a href="thesisDownload">Thesis</a></li>
                    <li><a href="journalDownload">Journal</a></li>
                    <li><a href="noteDownload">Note</a></li>
                    <li><a href="questionDownload">Exercise</a></li>
                    <li><a href="publicationDownload">Publication</a></li>
                    <li><a href="formsDownload">Forms</a></li>
                </ul>
            </li>
            
            <!-- <li class="nav-label">Upload</li>
            <li><a href="thesisUpload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Thesis</span></a></li>
            <li><a href="journalUpload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Journal</span></a></li> -->
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Upload</span></a>
                <ul aria-expanded="false">
                    <li><a href="thesisUpload">Thesis</a></li>
                    <li><a href="journalUpload">Journal</a></li>
                    <li><a href="noteUpload">Note</a></li>
                    <li><a href="questionUpload">Exercise</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Forum</span></a>
                <ul aria-expanded="false">
                    <li><a href="viewForum">All Forum</a></li>
                    <li><a href="myForum">My Forum</a></li>
                </ul>
            </li>

            <li><a href="viewAlbum"><i class="mdi mdi-email-outline"></i><span class="nav-text">Album </span></a></li>
            <li><a href="myFiles"><i class="mdi mdi-email-outline"></i><span class="nav-text">My Uploads </span></a></li>

        </ul>
    </div>
</div>