<script src="../assets/plugins/common/common.min.js"></script>
<script src="../assets/js/custom.min.js"></script>
<script src="../assets/js/settings.js"></script>
<script src="../assets/js/gleek.js"></script>
<script src="../assets/js/styleSwitcher.js"></script>

<script src="../assets/plugins/moment/moment.min.js"></script>
<script src="../assets/plugins/chart.js/Chart.bundle.min.js"></script>
<script src="../assets/plugins/chartjs-plugin-streaming/chartjs-plugin-streaming.min.js"></script>
<script src="../assets/js/dashboard/dashboard-10.js"></script>
<script src="../assets/js/plugins-init/form-bootstrap-validate-init.js"></script>
<script src="../assets/plugins/dropify/dist/js/dropify.min.js"></script>
<script src="../assets/js/plugins-init/dropify-init.js"></script>
<script src="../assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="../assets/js/plugins-init/datatables.init.js"></script>
<script src="../assets/plugins/bootstrap4-notify/bootstrap-notify.min.js"></script>
<script src="../assets/js/plugins-init/bootstrap-notify-init.js"></script>
<script src="../assets/plugins/webticker/jquery.webticker.min.js"></script>
<script src="../assets/js/plugins-init/webticker-init.js"></script>
<script src="../assets/plugins/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
<script src="../assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js"></script>
<script src="../assets/js/plugins-init/jquery.bootgrid-init.js"></script>
<script type="text/javascript">
    function onWindowClosing() {
        if (window.event.clientX < 0 || window.event.clientY < 0) {
            $.ajax({
                type: "POST",
                url: "../process/logout"
            });
        }
    };

    function onKeydown(evt) {
        if (evt != undefined && evt.altKey && evt.keyCode == 115) //Alt + F4 
        {
            $.ajax({
                type: "POST",
                url: "../process/logout"
            });
        }
    };

    window.onbeforeunload = onWindowClosing;
    window.document.onkeydown = onKeydown;
</script>