<!DOCTYPE html>
<?php 
session_start();
include("../process/inc_db.php");
include("../process/user_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Album / Photo Gallery</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                <?php
                    

                    $sql="SELECT * FROM album GROUP BY album_title";

                    if ($result=mysqli_query($con,$sql)){
                        // Fetch one and one row
                        while ($row=mysqli_fetch_array($result)){

                                $files = glob('../uploads/albums/'.$row['album_dir'] . '/*.*');
                                $file = array_rand($files);
                                
                                include("../process/album_detail.php");
                                
                            
                ?>
                    <div class="col-md-6 col-xl-4">
                        <div class="card">
                            <div class="position-relative">
                                <img alt="" src="<?php echo $files[$file]; ?>" class="img-fluid img-full" style="width:496px;height:286px;">                                
                            </div>
                            <div class="card-body">
                                <a href="singleAlbum?id=<?php echo base64_encode($row['album_title']); ?>">
                                    <h4 class="card-title"><?php echo $row['album_title']; ?></h4>
                                </a>
                                <p class="card-text"><?php echo mb_strimwidth("$description", 0, 100, "..."); ?></p>
                            </div>
                            <div class="card-footer py-4 px-xl-5">
                                <p class="card-text d-inline"><span class="text-muted"><?php echo $upload_date ?></span>
                                </p>                                
                            </div>
                        </div>
                    </div>
                <?php
                        }
                        // Free result set
                        mysqli_free_result($result);
                    }
                ?>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>