<!DOCTYPE html>
<?php 
session_start();
include("../process/inc_db.php");
include("../process/user_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Album / Photo Gallery</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                <?php
                include("../process/album_detail.php");
                    
                    $album_title = base64_decode($_GET['id']);

                    $sql="SELECT * FROM album WHERE album_title='$album_title'";

                    if ($result=mysqli_query($con,$sql)){
                        // Fetch one and one row
                        while ($row=mysqli_fetch_array($result)){   
                            
                            
                            
                ?>
                    

                    <div class="col-xl-3 col-lg-4 col-xxl-4 col-sm-6">
                        <div class="card card-portfolio-widget">
                            <img class="img-fluid" src="../uploads/albums/<?php echo $row['album_dir'] ?>/<?php echo $row['file_name'] ?>" alt="">                            
                        </div>
                    </div>
                <?php
                        }
                        // Free result set
                        mysqli_free_result($result);
                    }
                ?>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>