<!DOCTYPE html>
<?php 
session_start(); 
include("../process/user_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Note Upload</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-5 card-title">Owner Details</h4>
                                <form class="needs-validation" action="../process/upload_note" method="post" enctype="multipart/form-data" novalidate>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01">Fullname</label>
                                            <input type="text" class="form-control" id="validationCustom01" value="<?php echo $full_name; ?>" disabled required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom02">Email</label>
                                            <input type="text" name="owner_email" class="form-control" id="validationCustom02" value="<?php echo $_SESSION['email']; ?>" disabled required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustomUsername">Username</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="text" name="username" class="form-control" id="validationCustomUsername" value="<?php echo $_SESSION['username']; ?>" aria-describedby="inputGroupPrepend" disabled required>
                                                <div class="invalid-feedback">Please choose a username.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <h4 class="mb-5 card-title">Note Details</h4>
                                    <div class="form-row">
                                    
                                    
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom03">Note Title</label>
                                            <input type="text" name="note_title" class="form-control text-capitalize" id="validationCustom03" placeholder="Note Title" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Category</label>
                                            <select name="category" id="" required class="form-control">
                                                <option value=""></option>
                                                <?php include("../process/menu_list.php"); ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please select category
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <br/>
                                            <h4 class="card-title mb-5">Note Drop File</h4>
                                            <p class="mb-5">Only file type "<a class="text-info">DOC, DOCX</a>, <a class="text-danger">PPT, PPTX</a>, <a class="text-success">XLSX, XLS</a>, <a class="text-warning">PDF</a>"</p>
                                            <div class="dropify-default">
                                                <input type="file" name="note" class="dropify" data-allowed-file-extensions="pdf doc docx xls xlsx ppt pptx" />
                                            </div>  
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <br/>
                                            <div class="form-group pl-4">
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                                <label class="form-check-label ml-3" for="invalidCheck">
                                                    Agree to terms and conditions
                                                </label>
                                                <div class="invalid-feedback">
                                                    You must agree before submitting.
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary bs-submit" name="submit" type="submit">Upload Note</button>
                                    <button class="btn btn-danger" type="button">Cancel</button>        
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>