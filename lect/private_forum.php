<form class="needs-validation" id="private" action="../process/save_forum" method="post" novalidate>
    <input type="hidden" name="type" value="private">
    <div class="form-row">
        <div class="col-md-6 mb-3">
            <label for="validationCustom01">Fullname</label>
            <input type="text" name="moderator_name" class="form-control" id="validationCustom01" value="<?php echo $full_name; ?>" readonly required>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustom02">Email</label>
            <input type="text" name="moderator_email" class="form-control" id="validationCustom02" value="<?php echo $_SESSION['email']; ?>" readonly required>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustomUsername">Username</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <input type="text" name="username" class="form-control" id="validationCustomUsername" value="<?php echo $_SESSION['username']; ?>" aria-describedby="inputGroupPrepend" readonly required>
                <div class="invalid-feedback">Please choose a username.</div>
            </div>
        </div>
    </div>
    <br/>
    <h4 class="mb-5 card-title">Forum Details</h4>
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label for="validationCustom03">Forum Title</label>
            <input type="text" name="forum_title" class="form-control text-capitalize" id="validationCustom03" placeholder="Forum Title" required>
            <div class="invalid-feedback">
                Please Fill!
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="category1">Category</label>
            <select name="forum_category" id="category1" class="form-control" required>
                <option value=""></option>
                <?php include("../process/forum_category.php"); ?>
            </select>
            <div class="invalid-feedback">
                Please select category
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="parti1">Participant Number</label>
            <input type="text" name="number_participant" placeholder="Number of participant" class="form-control shouldNumber" id="parti1" required>
            <div class="invalid-feedback">
                Please write a number participant!
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <label for="description1">Forum Description</label>
            <textarea name="description" rows="4" placeholder="Please write description here" class="form-control" id="description1" required></textarea>
            <div class="invalid-feedback">
                Please write description here
            </div>
        </div>
    </div>
    <h4 class="mb-5 card-title">Forum Security</h4>
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label for="forum_id1">Forum ID</label>
            <input type="text" name="forum_id" class="form-control" id="forum_id1" value="<?php echo randStrGen(15); ?>" readonly required>
            <div class="invalid-feedback">
                Please Fill!
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="pass">Forum Password</label>
            <input type="password" name="password" placeholder="Forum password" class="form-control" id="pass" required>
            <div class="invalid-feedback">
                Please enter password
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="pass1">Confirm Password</label>
            <input type="password" name="password1" placeholder="Confirm password" class="form-control" id="pass1" required>
            <div class="invalid-feedback">
                Re-enter Password
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <br/>
            <div class="form-group pl-4">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                <label class="form-check-label ml-3" for="invalidCheck">
                    Agree to terms and conditions
                </label>
                <div class="invalid-feedback">
                    You must agree before submitting.
                </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary bs-submit" form="private" type="submit">Create Private</button>
</form>