<!DOCTYPE html>
<?php 
session_start(); 
include("../process/lect_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-5 card-title">Personal Details</h4>
                                <form class="needs-validation" action="../process/update_profile" method="post" enctype="multipart/form-data" novalidate>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01">Fullname</label>
                                            <input type="text" class="form-control text-capitalize" name="full_name" id="validationCustom01" value="<?php echo $full_name; ?>" required>
                                            <div class="invalid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom02">Email</label>
                                            <input type="text" name="email" class="form-control" id="validationCustom02" value="<?php echo $_SESSION['email']; ?>" readonly required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustomUsername">Username</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="text" name="username" class="form-control" id="validationCustomUsername" value="<?php echo $_SESSION['username']; ?>" aria-describedby="inputGroupPrepend" readonly required>
                                                <div class="invalid-feedback">Please choose a username.</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="mykad">MyKad Number</label>
                                            <input type="text" name="mykad" class="form-control inputmasl" data-mask="999999-99-9999" id="mykad" value="<?php echo $mykad; ?>" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="dob">Date of Birth</label>
                                            <input type="date" name="dob" class="form-control" id="dob" value="<?php echo $dob; ?>" required>
                                            <div class="invalid-feedback">
                                                Please Select Date!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Gender</label>
                                            <select name="gender" id="" required class="form-control">
                                                <option value="<?php echo $gender; ?>"><?php echo $gender; ?></option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please select gender
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="phone">Phone</label>
                                            <input id="phone" type="text" name="phone" class="form-control inputmask" data-mask="999-9999 9999" placeholder="Phone" value="<?php echo $phone; ?>" required>
                                            <div class="invalid-feedback">
                                                Please Insert Phone Number
                                            </div>
                                        </div>
                                        <?php if($_SESSION['role']=="staff"){
                                        ?>
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom01">Department</label>
                                                <input type="text" class="form-control text-capitalize" name="department" id="validationCustom01" value="<?php echo $department; ?>" required>
                                                <div class="invalid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div> 
                                    
                                    <div class="col-md-6">
                                            <br/>
                                            <h4 class="card-title mb-5">Profile Picture</h4>
                                            <p class="mb-5">Only file type "<a class="text-info">JPG only</a>"</p>
                                            <div class="dropify-default">
                                                <input type="file" name="profile_picture" class="dropify" value="../<?php echo $profile_pic; ?>" data-allowed-file-extensions="jpg JPG" />
                                            </div>  
                                        </div>                                    
                                    <br/>
                                    <div class="form-row">                                                          
                                        <div class="col-md-12 mb-3">
                                            <br/>
                                            <div class="form-group pl-4">
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                                <label class="form-check-label ml-3" for="invalidCheck">
                                                    Agree to terms and conditions
                                                </label>
                                                <div class="invalid-feedback">
                                                    You must agree before submitting.
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary bs-submit" name="type" value="<?php echo $_SESSION['role']; ?>" type="submit">Update Profile</button>
                                    <button class="btn btn-danger" type="button">Cancel</button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>