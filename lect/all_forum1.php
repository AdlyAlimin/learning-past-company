<div class="row">
    <?php
        $sql1="SELECT * FROM forum ORDER BY date_create DESC LIMIT 20";

        if ($result1=mysqli_query($con,$sql1))
        {
        // Fetch one and one row
        while ($row1=mysqli_fetch_array($result1))
            {
                include("../process/moderator_detail.php");
    ?>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="border-bottom-1 pb-3 d-sm-flex justify-content-between align-items-center">
                        <div class="bg-white">
                            <h3 class="text-uppercase"><?php echo $row1['forum_title']; ?></h3>
                        </div>
                        <div class="avatar-group my-3 my-sm-0">
                            <button type="button" class="btn btn-xs btn-success btn-rounded"><?php echo $row1['forum_type']; ?></button>
                            <button type="button" class="btn btn-xs btn-primary btn-rounded" onclick="window.location='#';"><?php echo $row1['forum_category']; ?></button>
                            <!-- <a href="javascript:void()" class="btn btn-xs btn-success btn-rounded"><?php echo $row1['forum_type']; ?></a> &nbsp;
                            <a href="javascript:void()" class="btn btn-xs btn-primary btn-rounded"><?php echo $row1['forum_category']; ?></a> &nbsp; -->
                            <?php
                                if(($row1['participant']+1)<=$row1['number_participant']){
                                    if($row1['forum_type']=="Private"){
                            ?>
                                        <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum_pass">+ Join Now</button>
                                        <div class="modal fade" id="forum_pass">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Forum Password</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form class="needs-validation1" action="../process/save_forum" method="post" novalidate>
                                                            <input type="hidden" name="type" value="public">
                                                            <div class="form-group">
                                                                <label class="text-label">Password*</label>
                                                                <div class="input-group transparent-append">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text" id="inputGroupPrepend3"> <i class="fa fa-lock" aria-hidden="true"></i> </span>
                                                                    </div>
                                                                    <input type="password" name="password" class="form-control" id="validationDefaultUsername11" placeholder="Password" aria-describedby="inputGroupPrepend3" required>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-xs btn-primary btn-rounded">Save changes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php
                                    }else{
                            ?>
                                        <!-- <a href="javascript:void()" class="btn btn-xs btn-info btn-rounded">+ Join Now</a> -->
                                        <!-- <button type="button" class="btn btn-xs btn-info btn-rounded">+ Join Now</button> -->
                                        <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum">+ Join Now</button>
                                        <div class="modal fade" id="forum">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Confirm Join</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="../process/join_forum" method="post">
                                                        <input type="hidden" name="forum_type" value="public">
                                                        <input type="hidden" name="forum_id" value="<?php echo $row1['forum_id']; ?>">
                                                        <p>Confirm to Join?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-xs btn-primary btn-rounded">Join Now</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                                else{
                            ?>
                                    <!-- <a href="javascript:void()" class="btn btn-xs btn-warning btn-rounded">View</a> -->
                                    <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo $row1['forum_id']; ?>'">View</button>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row mt-4 align-items-center">
                        <div class="col-4"><span class="text-muted f-s-12">Allow Participant</span>
                            <h2 class="text-primary"><?php echo $row1['number_participant']; ?></h2>
                        </div>
                        <div class="col-4"><span class="text-muted f-s-12">Moderator</span>
                            <h2 class="text-primary"><?php echo $moderator_name ?></h2>
                        </div>
                        <div class="col-4"><span class="text-muted f-s-12">Participant</span>
                            <h2 class="text-primary"><?php echo $row1['participant']; ?></h2>
                        </div>
                        <div class="col-12"><span class="text-muted f-s-12">Description</span>
                            <p><?php echo mb_strimwidth("$row1[description]", 0, 100, "...");  ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php                     
            }
        // Free result set
        mysqli_free_result($result1);
        }
    ?>
</div>