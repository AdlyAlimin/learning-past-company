<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="display" style="min-width: 845px">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Moderator</th>
                                <th>Forum Type</th>
                                <th>Category</th>
                                <th>Participant</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php  
                            $sql1="SELECT * FROM forum WHERE status='Show' ORDER BY date_create DESC";

                            if ($result1=mysqli_query($con,$sql1))
                            {
                                $i = 1;
                                // Fetch one and one row
                                while ($row1=mysqli_fetch_array($result1))
                                {
                                    include("../process/moderator_detail.php");
                        ?>
                                    <tr>
                                        <td data-toggle="tooltip" data-placement="right" title="<?php echo $row1['forum_title'] ?>"><?php echo mb_strimwidth("$row1[forum_title]", 0, 50, "..."); ?></td>
                                        <td data-toggle="tooltip" data-placement="right" title="<?php echo $row1['description'] ?>"><?php echo mb_strimwidth("$row1[description]", 0, 50, "...");  ?></td>
                                        <td><?php echo $moderator_name ?></td>
                                        <td><?php echo $row1['forum_type']; ?></td>
                                        <td><?php echo $row1['forum_category']; ?></td>
                                        <td><?php echo $row1['participant']; ?></td>
                                        <td>                                    
                                            <?php
                                                if(($row1['participant']+1)<=$row1['number_participant']){
                                                    if($row1['forum_type']=="Private"){
                                            ?>
                                                        <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum_pass<?php echo $i ?>">+ Join Now</button>
                                                        <div class="modal fade" id="forum_pass<?php echo $i ?>">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Forum Password</h5>
                                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="needs-validation1" action="../process/join_forum" method="post" novalidate>
                                                                            <input type="hidden" name="forum_type" value="private">
                                                                            <input type="hidden" name="forum_id" value="<?php echo $row1['forum_id']; ?>">
                                                                            <div class="form-group">
                                                                                <label class="text-label">Password*</label>
                                                                                <div class="input-group transparent-append">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" id="inputGroupPrepend3"> <i class="fa fa-lock" aria-hidden="true"></i> </span>
                                                                                    </div>
                                                                                    <input type="password" name="password" class="form-control" id="validationDefaultUsername11" placeholder="Forum Password" aria-describedby="inputGroupPrepend3" required>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-xs btn-primary btn-rounded">Save changes</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo $row1['forum_id']; ?>'">View</button>
                                            <?php
                                                    }else{
                                            ?>
                                                        <!-- <a href="javascript:void()" class="btn btn-xs btn-info btn-rounded">+ Join Now</a> -->
                                                        <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum<?php echo $i ?>">+ Join Now</button>
                                                        <div class="modal fade" id="forum<?php echo $i ?>">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Confirm Join</h5>
                                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action="../process/join_forum" method="post">
                                                                        <input type="hidden" name="forum_type" value="public">
                                                                        <input type="hidden" name="forum_id" value="<?php echo $row1['forum_id']; ?>">
                                                                        <p>Confirm to Join?</p>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-xs btn-primary btn-rounded">Join Now</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo htmlentities($row1['forum_id']); ?>'">View</button>
                                            <?php
                                                    }
                                                }else{
                                            ?>
                                                    <!-- <a href="javascript:void()" class="btn btn-xs btn-warning btn-rounded">View</a> -->
                                                    <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo $row1['forum_id']; ?>'">View</button>
                                            <?php
                                                }
                                            ?>
                                        </td>
                                    </tr>
                        <?php       
                                $i++;
                                }
                            // Free result set
                            mysqli_free_result($result1);
                            }
                        ?>                                      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>