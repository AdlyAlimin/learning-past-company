<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="display" style="min-width: 845px">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Moderator</th>
                                <th>Forum Type</th>
                                <th>Category</th>
                                <th>Participant</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php  
                            $sql1="SELECT * FROM forum,participant WHERE participant.student_email='$_SESSION[email]' AND forum.forum_id=participant.forum_id ORDER BY participant.date_join DESC";

                            if ($result1=mysqli_query($con,$sql1))
                            {
                                $i = 1;
                                // Fetch one and one row
                                while ($row1=mysqli_fetch_array($result1))
                                {
                                    include("../process/moderator_detail.php");
                        ?>
                                    <tr>
                                        <td data-toggle="tooltip" data-placement="right" title="<?php echo $row1['forum_title'] ?>"><?php echo mb_strimwidth("$row1[forum_title]", 0, 50, "..."); ?></td>
                                        <td data-toggle="tooltip" data-placement="right" title="<?php echo $row1['description'] ?>"><?php echo mb_strimwidth("$row1[description]", 0, 50, "...");  ?></td>
                                        <td><?php echo $moderator_name ?></td>
                                        <td><?php echo $row1['forum_type']; ?></td>
                                        <td><?php echo $row1['forum_category']; ?></td>
                                        <td><?php echo $row1['participant']; ?></td>
                                        <td><button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo $row1['forum_id']; ?>'">View</button></td>
                                    </tr>
                        <?php       
                                $i++;
                                }
                            // Free result set
                            mysqli_free_result($result1);
                            }
                        ?>                                      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>