<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>IS4L - Information Sharing for Learning</title>
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
<!-- Custom Stylesheet -->
<link href="../assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/plugins/dropify/dist/css/dropify.min.css">
<link href="../assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/plugins/owl.carousel/dist/css/owl.carousel.min.css">