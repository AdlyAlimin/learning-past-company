<!DOCTYPE html>
<?php 
session_start();
include("../process/lect_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">All My Uploads</li>
                        </ol>
                    </div>
                </div>
    

                <div class="row">
                <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">All My Uploads</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example-advance-1" class="display" style="min-width: 845px">
                                    <thead>
                                        <tr>
                                            <th>File Title</th>
                                            <th>Category</th>
                                            <th>Upload Date</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sql1="SELECT * FROM journal WHERE owner_email='$_SESSION[email]'";

                                        if ($result1=mysqli_query($con,$sql1))
                                        {
                                            
                                            $i = 1;
                                            // Fetch one and one row
                                            while ($row1=mysqli_fetch_array($result1))
                                            {
                                                
                                    ?>
                                        <tr>
                                            <td><?php echo $row1['journal_title']; ?></td>
                                            <td><?php echo $row1['category']; ?></td>
                                            <td><?php echo $row1['upload_date']; ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#journal<?php echo $i ?>">Delete</button>
                                                <div class="modal fade" id="journal<?php echo $i ?>">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Forum Password</h5>
                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation1" action="../process/delete_files" method="post" novalidate>
                                                                    <input type="hidden" name="type" value="<?php echo base64_encode('journal'); ?>">
                                                                    <input type="hidden" name="dtype" value="<?php echo base64_encode($row1['type']); ?>">
                                                                    <input type="hidden" name="title" value="<?php echo base64_encode($row1['journal_title']); ?>">
                                                                    <p>Sure Detele this Files ?</p>
                                                                    <p><?php echo $row1['journal_title']; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-xs btn-warning btn-rounded">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php                
                                                $i++;     
                                            }
                                        }
                                    
                                        $sql2="SELECT * FROM thesis WHERE owner_email='$_SESSION[email]'";

                                        if ($result2=mysqli_query($con,$sql2))
                                        {
                                            $i = 1;
                                            // Fetch one and one row
                                            while ($row2=mysqli_fetch_array($result2))
                                            {
                                                
                                    ?>
                                        <tr>
                                            <td><?php echo $row2['thesis_title']; ?></td>
                                            <td><?php echo $row2['category']; ?></td>
                                            <td><?php echo $row2['upload_date']; ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#thesis<?php echo $i ?>">Delete</button>
                                                <div class="modal fade" id="thesis<?php echo $i ?>">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Forum Password</h5>
                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation1" action="../process/delete_files" method="post" novalidate>
                                                                    <input type="hidden" name="type" value="<?php echo base64_encode('thesis'); ?>">
                                                                    <input type="hidden" name="dtype" value="<?php echo base64_encode($row2['type']); ?>">
                                                                    <input type="hidden" name="title" value="<?php echo base64_encode($row2['thesis_title']); ?>">
                                                                    <p>Sure Detele this Files ?</p>
                                                                    <p><?php echo $row2['thesis_title']; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-xs btn-warning btn-rounded">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php   
                                                $i++;    
                                            }
                                        }

                                        $sql3="SELECT * FROM note WHERE owner_email='$_SESSION[email]'";

                                        if ($result3=mysqli_query($con,$sql3))
                                        {
                                            $i = 1;
                                            // Fetch one and one row
                                            while ($row3=mysqli_fetch_array($result3))
                                            {
                                    ?>
                                        <tr>
                                            <td><?php echo $row3['note_title']; ?></td>
                                            <td><?php echo $row3['category']; ?></td>
                                            <td><?php echo $row3['upload_date']; ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#note<?php echo $i ?>">Delete</button>
                                                <div class="modal fade" id="note<?php echo $i ?>">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Forum Password</h5>
                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation1" action="../process/delete_files" method="post" novalidate>
                                                                    <input type="hidden" name="type" value="<?php echo base64_encode('note'); ?>">
                                                                    <input type="hidden" name="dtype" value="<?php echo base64_encode($row3['type']); ?>">
                                                                    <input type="hidden" name="title" value="<?php echo base64_encode($row3['note_title']); ?>">
                                                                    <p>Sure Detele this Files ?</p>
                                                                    <p><?php echo $row3['note_title']; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-xs btn-warning btn-rounded">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php       $i++; 
                                            }                                        
                                        }

                                        $sql4="SELECT * FROM question WHERE owner_email='$_SESSION[email]'";

                                        if ($result4=mysqli_query($con,$sql4))
                                        {
                                            $i = 1;
                                            // Fetch one and one row
                                            while ($row4=mysqli_fetch_array($result4))
                                            {
                                    ?>
                                        <tr>
                                            <td><?php echo $row4['question_title']; ?></td>
                                            <td><?php echo $row4['category']; ?></td>
                                            <td><?php echo $row4['upload_date']; ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#quest<?php echo $i ?>">Delete</button>
                                                <div class="modal fade" id="quest<?php echo $i ?>">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Forum Password</h5>
                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation1" action="../process/delete_files" method="post" novalidate>
                                                                    <input type="hidden" name="type" value="<?php echo base64_encode('question'); ?>">
                                                                    <input type="hidden" name="dtype" value="<?php echo base64_encode($row4['type']); ?>">
                                                                    <input type="hidden" name="title" value="<?php echo base64_encode($row4['question_title']); ?>">
                                                                    <p>Sure Detele this Files ?</p>
                                                                    <p><?php echo $row4['question_title']; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-xs btn-warning btn-rounded">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php       $i++;
                                            }
                                        // Free result set
                                        mysqli_free_result($result1);
                                        mysqli_free_result($result2);
                                        mysqli_free_result($result3);
                                        mysqli_free_result($result4);
                                        }
                                    ?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>