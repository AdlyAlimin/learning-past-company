<!DOCTYPE html>
<?php 
session_start();
include("../process/lect_detail.php");
include("../process/forum_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Forum</li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">

                    <div class="col-md-6">                        
                        <div class="card story-widget">
                            <div class="card-header border-bottom d-flex justify-content-between">
                                <h4 class="heading-primary"><?php echo $forum_title ?></h4>
                                <p class="font-small"><?php echo $date_create ?></p>
                            </div>
                            <div class="card-body">
                                <p><?php echo $description ?></p>
                                <div class="reaction-section d-flex mb-3">
                                    <div class="comment">
                                        <i class="fa fa-comments" aria-hidden="true"></i>
                                        <span class="count mr-4 ml-2"><?php echo date("Y-m-d H:i:s", strtotime('+24 hours')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group mt-5 mb-0 absolute-icon-append-widget">
                                    <form action="../process/save_comment_lect" method="post">
                                        <span class="absolute-form-icon absolute-form-icon-append">
                                            <i class="fa fa-comment" aria-hidden="true"></i>
                                        </span>
                                        <input type="hidden" name="forum_id" value="<?php echo $forum_id ?>">
                                        <input type="text" name="comment" placeholder="Wtite your comments" name="comment3" id="comment3" class="form-control bg-white">
                                        <br/>
                                        <button class="btn btn-xs btn-success btn-rounded" name="submit" type="submit">Place Comment</button>
                                    </form>
                                </div>
                                
                                <?php include("../process/student_comment.php"); ?>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-6">                        
                        <div class="card story-widget">
                            <div class="card-header border-bottom d-flex justify-content-between">
                                <h4 class="heading-primary">Forum Setting</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" action="../process/update_forum" method="post" novalidate>
                                    <input type="hidden" name="forum_id" value="<?php echo $forum_id ?>">
                                    <div class="form-row">
                                        <div class="col-md-12 mb-12">
                                            <label for="validationCustom02">Forum Title</label>
                                            <input type="text" name="forum_title" class="form-control text-capitalize" id="validationCustom02" value="<?php echo $forum_title ?>" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-6">
                                            <label for="category1">Category</label>
                                            <select name="forum_category" id="category1" class="form-control" required>
                                                <option value="<?php echo $forum_category ?>">Current : <?php echo $forum_category ?></option>
                                                <?php include("../process/forum_category.php"); ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please select category
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-6">
                                            <label for="parti1">Participant Number</label>
                                            <input type="text" name="number_participant" placeholder="Number of participant" class="form-control shouldNumber" id="parti1" value="<?php echo $number_participant ?>" required>
                                            <div class="invalid-feedback">
                                                Please write a number participant!
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label for="description1">Forum Description</label>
                                            <textarea name="description" rows="4" placeholder="Please write description here" class="form-control" id="description1" required><?php echo $description ?></textarea>
                                            <div class="invalid-feedback">
                                                Please write description here
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <label for="description1">Forum Available: <b><?php echo $status; ?></b></label>
                                            <div class="form-radio">
                                            <input id="Show" class="radio-outlined"  name="status" <?=$status=="Show" ? "checked" : ""?> value="Show" type="radio">
                                            <label for="Show" class="radio-green">Show</label>
                                            <input id="Hide" class="radio-outlined"  name="status" <?=$status=="Hide" ? "checked" : ""?> value="Hide" type="radio">
                                            <label for="Hide" class="radio-green">Hide</label>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <hr>
                                    <button class="btn btn-primary bs-submit" name="submit" type="submit">Update Forum</button>
                                </form>
                            </div>
                        </div> 
                    </div>

                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>