<!DOCTYPE html>
<?php 
session_start();
include("../process/lect_detail.php");
include("../count/thesis_count.php");
include("../count/journal_count.php");
include("../count/forum_count.php");
include("../count/note_count.php");
include("../count/question_count.php");
include("../count/publication_count.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="bootstrap-carousel">
                                    <div data-ride="carousel" class="carousel slide" id="carouselExampleCaptions">                                        
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block w-100" src="../assets/images/banner.png" alt="" style="height:300px;">
                                                <div class="carousel-caption d-none d-md-block">
                                                    <h5>New Welcome</h5>
                                                </div>
                                            </div>
                                        <?php
                                            $sql1="SELECT * FROM banner ORDER BY date_upload";

                                            if ($result1=mysqli_query($con,$sql1))
                                            {
                                            // Fetch one and one row
                                            while ($row1=mysqli_fetch_array($result1))
                                                {
                                        ?>
                                                    <div class="carousel-item">
                                                        <img class="d-block w-100" src="../<?php echo $row1['banner_path']; ?>" alt="" style="height:300px;">
                                                        <div class="carousel-caption d-none d-md-block">
                                                            <h5><?php echo $row1['banner_title']; ?></h5>
                                                        </div>
                                                    </div>
                                        <?php                     
                                                }
                                            // Free result set
                                            mysqli_free_result($result1);
                                            }
                                        ?>
                                        </div><a data-slide="prev" href="#carouselExampleCaptions" class="carousel-control-prev"><span
                                                class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span>
                                        </a><a data-slide="next" href="#carouselExampleCaptions" class="carousel-control-next"><span
                                                class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="crypto-ticker card">
                            <div class="ticker-overview">Top News</div>
                            <ul id="webticker-dark-icons">
                                <?php include("../process/news.php"); ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card flex-row download-docs-card flex-wrap">
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/newicon/all.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $thesisCount; ?></h2>
                                        <p>Thesis</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/newicon/all.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $journalCount; ?></h2>
                                        <p>Journal</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/newicon/all.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $noteCount; ?></h2>
                                        <p>Note</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/newicon/all.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $questionCount; ?></h2>
                                        <p>Exercise</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/newicon/all.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $publicationCount; ?></h2>
                                        <p>Publication</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-4 col-lg-12 col-xxl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Point List</h4>
                                <p class="text-muted"><code></code>
                                </p>
                                <div id="accordion-one" class="accordion">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fa" aria-hidden="true"></i>Student Top 10</h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" data-parent="#accordion-one">                                            
                                            <?php include("../process/point.php"); ?>                                            
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fa" aria-hidden="true"></i>Lecturer Top 10</h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" data-parent="#accordion-one">                                            
                                            <?php include("../process/lect_point.php"); ?>                                            
                                        </div>
                                    </div>                                     
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="fa" aria-hidden="true"></i>Staff Top 10</h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" data-parent="#accordion-one">                                            
                                            <?php include("../process/staff_point.php"); ?>                                            
                                        </div>
                                    </div>                                                                       
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-8 col-lg-12 col-xxl-12">

                        <div class="card">
                            <div class="card-body widget-file-container">
                                <h4 class="card-title">Thesis <a class="float-right text-info" href="javascript:void()"><small>Show All (<?php echo $thesisCount; ?>)</small></a></h4>
                                <div class="d-flex flex-wrap">
                                
                                <?php
                                    $sql1="SELECT * FROM thesis ORDER BY upload_date DESC LIMIT 20";

                                    if ($result1=mysqli_query($con,$sql1))
                                    {
                                    // Fetch one and one row
                                    while ($row1=mysqli_fetch_array($result1))
                                        {
                                ?> 
                                            <div class="file-container">
                                                <a href="../process/download?file=<?php echo base64_encode($row1['thesis_path']); ?>">
                                                    <?php include("../process/file_icon.php"); ?>
                                                </a>                                                
                                                <p><small><?php echo mb_strimwidth("$row1[thesis_title]", 0, 30, "..."); ?></small>
                                                </p>
                                            </div>
                                <?php                     
                                        }
                                    // Free result set
                                    mysqli_free_result($result1);
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card">
                            <div class="card-body widget-file-container">
                                <h4 class="card-title">Journal <a class="float-right text-info" href="javascript:void()"><small>Show All (<?php echo $journalCount; ?>)</small></a></h4>
                                <div class="d-flex flex-wrap">
                                
                                <?php
                                    $sql1="SELECT * FROM journal ORDER BY upload_date DESC LIMIT 20";

                                    if ($result1=mysqli_query($con,$sql1))
                                    {
                                    // Fetch one and one row
                                    while ($row1=mysqli_fetch_array($result1))
                                        {
                                ?>
                                            <div class="file-container">
                                                <a href="../process/download?file=<?php echo base64_encode($row1['journal_path']); ?>">
                                                    <?php include("../process/file_icon.php"); ?>
                                                </a>
                                                <p><small><?php echo mb_strimwidth("$row1[journal_title]", 0, 30, "..."); ?></small>
                                                </p>
                                            </div>
                                <?php                     
                                        }
                                    // Free result set
                                    mysqli_free_result($result1);
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
 
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-xxl-6">
                        <div class="card">
                            <div class="card-body widget-file-container">
                                <h4 class="card-title">Note <a class="float-right text-info" href="javascript:void()"><small>Show All (<?php echo $noteCount; ?>)</small></a></h4>
                                <div class="d-flex flex-wrap">
                                
                                <?php
                                    $sql1="SELECT * FROM note ORDER BY upload_date DESC LIMIT 20";

                                    if ($result1=mysqli_query($con,$sql1))
                                    {
                                    // Fetch one and one row
                                    while ($row1=mysqli_fetch_array($result1))
                                        {
                                ?>
                                            <div class="file-container">
                                                <a href="../process/download?file=<?php echo base64_encode($row1['note_path']); ?>">
                                                    <?php include("../process/file_icon.php"); ?>
                                                </a>                                                
                                                <p><small><?php echo mb_strimwidth("$row1[note_title]", 0, 30, "..."); ?></small>
                                                </p>
                                            </div>
                                <?php                     
                                        }
                                    // Free result set
                                    mysqli_free_result($result1);
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-6 col-lg-6 col-xxl-6">    
                        <div class="card">
                            <div class="card-body widget-file-container">
                                <h4 class="card-title">Exercise <a class="float-right text-info" href="javascript:void()"><small>Show All (<?php echo $questionCount; ?>)</small></a></h4>
                                <div class="d-flex flex-wrap">
                                
                                <?php
                                    $sql1="SELECT * FROM question ORDER BY upload_date DESC LIMIT 20";

                                    if ($result1=mysqli_query($con,$sql1))
                                    {
                                    // Fetch one and one row
                                    while ($row1=mysqli_fetch_array($result1))
                                        {
                                ?>
                                            <div class="file-container">
                                                <a href="../process/download?file=<?php echo base64_encode($row1['question_path']); ?>">
                                                    <?php include("../process/file_icon.php"); ?>
                                                </a>
                                                <p><small><?php echo mb_strimwidth("$row1[question_title]", 0, 30, "..."); ?></small>
                                                </p>
                                            </div>
                                <?php                     
                                        }
                                    // Free result set
                                    mysqli_free_result($result1);
                                    }
                                ?>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col-xl-6 col-lg-6 col-xxl-6">    
                        <div class="card">
                            <div class="card-body widget-file-container">
                                <h4 class="card-title">Publication <a class="float-right text-info" href="javascript:void()"><small>Show All (<?php echo $publicationCount; ?>)</small></a></h4>
                                <div class="d-flex flex-wrap">
                                
                                <?php
                                    $sql1="SELECT * FROM publication ORDER BY upload_date DESC LIMIT 20";

                                    if ($result1=mysqli_query($con,$sql1))
                                    {
                                    // Fetch one and one row
                                    while ($row1=mysqli_fetch_array($result1))
                                        {
                                ?>
                                            <div class="file-container">
                                                <a href="../process/download?file=<?php echo base64_encode($row1['publication_path']); ?>">
                                                    <?php include("../process/file_icon.php"); ?>
                                                </a>
                                                <p><small><?php echo mb_strimwidth("$row1[publication_title]", 0, 30, "..."); ?></small>
                                                </p>
                                            </div>
                                <?php                     
                                        }
                                    // Free result set
                                    mysqli_free_result($result1);
                                    }
                                ?>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                      
                <?php if($_SESSION['role']=="lect"){
                
                ?>
                    <div class="row">
                        <?php
                            $sql1="SELECT * FROM forum WHERE status='Show' ORDER BY date_create DESC LIMIT 10";

                            if ($result1=mysqli_query($con,$sql1))
                            {
                            // Fetch one and one row
                            while ($row1=mysqli_fetch_array($result1))
                                {
                                    include("../process/moderator_detail.php");
                        ?>
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="border-bottom-1 pb-3 d-sm-flex justify-content-between align-items-center">
                                            <div class="bg-white">
                                                <h3 class="text-uppercase"><?php echo mb_strimwidth("$row1[forum_title]", 0, 20, "..."); ?></h3>
                                            </div>
                                            <div class="avatar-group my-3 my-sm-0">
                                                <button type="button" class="btn btn-xs btn-success btn-rounded"><?php echo $row1['forum_type']; ?></button>
                                                <button type="button" class="btn btn-xs btn-primary btn-rounded" onclick="window.location='#';"><?php echo $row1['forum_category']; ?></button>
                                                <!-- <a href="javascript:void()" class="btn btn-xs btn-success btn-rounded"><?php echo $row1['forum_type']; ?></a> &nbsp;
                                                <a href="javascript:void()" class="btn btn-xs btn-primary btn-rounded"><?php echo $row1['forum_category']; ?></a> &nbsp; -->
                                                <?php
                                                    if(($row1['participant']+1)<=$row1['number_participant']){
                                                        if($row1['forum_type']=="Private"){
                                                ?>
                                                            <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum_pass">+ Join Now</button>
                                                            <div class="modal fade" id="forum_pass">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title">Forum Password</h5>
                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="needs-validation1" action="../process/join_forum" method="post" novalidate>
                                                                                <input type="hidden" name="type" value="private">
                                                                                <div class="form-group">
                                                                                    <label class="text-label">Password*</label>
                                                                                    <div class="input-group transparent-append">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text" id="inputGroupPrepend3"> <i class="fa fa-lock" aria-hidden="true"></i> </span>
                                                                                        </div>
                                                                                        <input type="password" name="password" class="form-control" id="validationDefaultUsername11" placeholder="Forum Password" aria-describedby="inputGroupPrepend3" required>
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-xs btn-primary btn-rounded">Save changes</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo $row1['forum_id']; ?>'">View</button>
                                                <?php
                                                        }else{
                                                ?>
                                                            <!-- <a href="javascript:void()" class="btn btn-xs btn-info btn-rounded">+ Join Now</a> -->
                                                            <button type="button" class="btn btn-xs btn-info btn-rounded" data-toggle="modal" data-target="#forum">+ Join Now</button>
                                                            <div class="modal fade" id="forum">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title">Confirm Join</h5>
                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="../process/join_forum" method="post">
                                                                            <input type="hidden" name="forum_type" value="public">
                                                                            <input type="hidden" name="forum_id" value="<?php echo $row1['forum_id']; ?>">
                                                                            <p>Confirm to Join?</p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-xs btn-primary btn-rounded">Join Now</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo htmlentities($row1['forum_id']); ?>'">View</button>
                                                <?php
                                                        }
                                                    }
                                                    else{
                                                ?>
                                                        <!-- <a href="javascript:void()" class="btn btn-xs btn-warning btn-rounded">View</a> -->
                                                        <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='singleForum?id=<?php echo htmlentities($row1['forum_id']); ?>'">View</button>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row mt-4 align-items-center">
                                            <div class="col-4"><span class="text-muted f-s-12">Allow Participant</span>
                                                <h2 class="text-primary"><?php echo $row1['number_participant']; ?></h2>
                                            </div>
                                            <div class="col-4"><span class="text-muted f-s-12">Moderator</span>
                                                <h2 class="text-primary"><?php echo $moderator_name ?></h2>
                                            </div>
                                            <div class="col-4"><span class="text-muted f-s-12">Participant</span>
                                                <h2 class="text-primary"><?php echo $row1['participant']; ?></h2>
                                            </div>
                                            <div class="col-12"><span class="text-muted f-s-12">Title</span>
                                                <p><?php echo mb_strimwidth("$row1[forum_title]", 0, 100, "...");  ?></p>
                                            </div>
                                            <div class="col-12"><span class="text-muted f-s-12">Description</span>
                                                <p><?php echo $row1['description']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php                     
                                }
                            // Free result set
                            mysqli_free_result($result1);
                            }
                        ?>
                    </div>
                <?php
                }else if($_SESSION['role']=="staff"){
                    
                }
                ?>
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>