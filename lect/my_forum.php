<div class="row">
<?php
    $sql1="SELECT * FROM forum WHERE moderator_email='$_SESSION[email]' ORDER BY date_create DESC";

    if ($result1=mysqli_query($con,$sql1))
    {
    // Fetch one and one row
    while ($row1=mysqli_fetch_array($result1))
        {
            include("../process/moderator_detail.php");
?>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="border-bottom-1 pb-3 d-sm-flex justify-content-between align-items-center">
                    <div class="bg-white">
                        <h3 class="text-uppercase"><?php echo mb_strimwidth("$row1[forum_title]", 0, 20, "..."); ?></h3>
                    </div>
                    <div class="avatar-group my-3 my-sm-0">
                        <button type="button" class="btn btn-xs btn-success btn-rounded"><?php echo $row1['forum_type']; ?></button>
                        <button type="button" class="btn btn-xs btn-primary btn-rounded"><?php echo $row1['forum_category']; ?></button>
                        <button type="button" class="btn btn-xs btn-warning btn-rounded" onclick="window.location='forumDetail?id=<?php echo htmlentities($row1['forum_id']); ?>'">View</button>
                    </div>
                </div>
                <div class="row mt-4 align-items-center">
                    <div class="col-4"><span class="text-muted f-s-12">Allow Participant</span>
                        <h2 class="text-primary"><?php echo $row1['number_participant']; ?></h2>
                    </div>
                    <div class="col-4"><span class="text-muted f-s-12">Moderator</span>
                        <h2 class="text-primary"><?php echo $moderator_name ?></h2>
                    </div>
                    <div class="col-4"><span class="text-muted f-s-12">Participant</span>
                        <h2 class="text-primary"><?php echo $row1['participant']; ?></h2>
                    </div>
                    <div class="col-12"><span class="text-muted f-s-12">Description</span>
                        <p><?php echo mb_strimwidth("$row1[description]", 0, 100, "...");  ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php                     
        }
    // Free result set
    mysqli_free_result($result1);
    }
?>
</div>