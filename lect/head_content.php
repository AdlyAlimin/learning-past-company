<div class="header">    
    <div class="header-content">
        <div class="header-right">
            <ul>
                <li class="icons">
                    <a href="javascript:void(0)" class="log-user">
                        <img src="../<?php echo $profile_pic ?>" alt="" style="width:50px;height:50px;"> <span><?php echo $full_name; ?></span>  <i class="fa fa-caret-down f-s-14" aria-hidden="true"></i>
                    </a>
                    <div class="drop-down dropdown-profile animated bounceInDown">
                        <div class="dropdown-content-body">
                            <ul>
                                <li><a href="profile"><i class="icon-user"></i> <span>My Profile</span></a>
                                </li>
                                <li><a href="../process/logout"><i class="icon-power"></i> <span>Logout</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>