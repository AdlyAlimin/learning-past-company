<form class="needs-validation1" action="../process/save_forum" method="post" novalidate>
    <input type="hidden" name="type" value="public">
    <div class="form-row">
        <div class="col-md-6 mb-3">
            <label for="moderator">Fullname</label>
            <input type="text" name="moderator_name" class="form-control" id="moderator" value="<?php echo $full_name; ?>" readonly required>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="mod_email">Email</label>
            <input type="text" name="moderator_email" class="form-control" id="mod_email" value="<?php echo $_SESSION['email']; ?>" readonly required>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="username">Username</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <input type="text" name="username" class="form-control" id="username" value="<?php echo $_SESSION['username']; ?>" aria-describedby="inputGroupPrepend" readonly required>
                <div class="invalid-feedback">Please choose a username.</div>
            </div>
        </div>
    </div>
    <br/>
    <h4 class="mb-5 card-title">Forum Details</h4>
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label for="forum">Forum Title</label>
            <input type="text" name="forum_title" class="form-control text-capitalize" id="forum" placeholder="Forum Title" required>
            <div class="invalid-feedback">
                Please Fill!
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="category">Category</label>
            <select name="forum_category" id="category" placeholder="Please select category" required class="form-control" required>
                <option value=""></option>
                <?php include("../process/forum_category.php"); ?>
            </select>
            <div class="invalid-feedback">
                Please select category
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="parti">Participant Number</label>
            <input type="text" name="number_participant" required placeholder="Number of participant" class="form-control shouldNumber" id="parti">
            <div class="invalid-feedback">
                Please write a number participant!
            </div>
        </div>
        <div class="col-md-12 mb-12">
            <label for="forum_id">Forum ID</label>
            <input type="text" name="forum_id" class="form-control" id="forum_id" value="<?php echo randStrGen(15); ?>" readonly required>
            <div class="invalid-feedback">
                Forum ID
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <label for="description">Forum Description</label>
            <textarea name="description" rows="4" placeholder="Please write description here" class="form-control" id="description" required></textarea>
            <div class="invalid-feedback">
                Please write description here
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <br/>
            <div class="form-group pl-4">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="check" required>
                <label class="form-check-label ml-3" for="check">
                    Agree to terms and conditions
                </label>
                <div class="invalid-feedback">
                    You must agree before submitting.
                </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary bs-submit1" name="public" type="submit">Create Public</button>
</form>