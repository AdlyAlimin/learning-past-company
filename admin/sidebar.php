<div class="nk-sidebar">           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Dashboard</li>
            <li><a href="home"><i class="mdi mdi-email-outline"></i><span class="nav-text">Homepage </span></a></li>
            <li><a href="news"><i class="mdi mdi-email-outline"></i><span class="nav-text">News </span></a></li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Users</span></a>
                <ul aria-expanded="false">
                    <li><a href="studentList">Student</a></li>
                    <li><a href="lectureList">Lecturer</a></li>
                    <li><a href="staffList">Staff</a></li>
                </ul>
            </li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Banner</span></a>
                <ul aria-expanded="false">
                    <li><a href="bannerUpload">Add Banner</a></li>
                    <li><a href="viewBanner">View Banner</a></li>
                </ul>
            </li>   
    
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Album / Photo</span></a>
                <ul aria-expanded="false">
                    <li><a href="albumUpload">Add Album</a></li>
                    <li><a href="viewAlbum">View Album</a></li>
                </ul>
            </li>
            
            <!-- <li class="nav-label">Download</li>
            <li><a href="thesisDownload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Thesis</span></a></li>
            <li><a href="journalDownload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Journal</span></a></li> -->
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Setting</span></a>
                <ul aria-expanded="false">
                    <li><a href="menuSetting">Category Setting</a></li>
                    <li><a href="forumCategory">Forum Category</a></li>
                    <li><a href="campusList">Campus List</a></li>
                    <li><a href="pointSetting">Point</a></li>
                    <li><a href="downloadSetting">Download Limit</a></li>
                </ul>
            </li>
            
            <!-- <li class="nav-label">Upload</li>
            <li><a href="thesisUpload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Thesis</span></a></li>
            <li><a href="journalUpload"><i class="mdi mdi-email-outline"></i><span class="nav-text">Journal</span></a></li> -->
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-email-outline"></i> <span class="nav-text">Upload</span></a>
                <ul aria-expanded="false">
                    <li><a href="formsUpload">Forms</a></li>
                </ul>
            </li> 

            <li><a href="myFiles"><i class="mdi mdi-email-outline"></i><span class="nav-text">My Files </span></a></li>           

        </ul>
    </div>
</div>