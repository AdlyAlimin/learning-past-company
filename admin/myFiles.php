<!DOCTYPE html>
<?php 
session_start();
include("../process/user_detail.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">All My Uploads</li>
                        </ol>
                    </div>
                </div>
    

                <div class="row">
                <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">All My Uploads</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example-advance-1" class="display" style="min-width: 845px">
                                    <thead>
                                        <tr>
                                            <th>File Title</th>
                                            <th>Upload Date</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php                                                                           

                                        $sql4="SELECT * FROM forms WHERE owner_email='$_SESSION[email]'";

                                        if ($result4=mysqli_query($con,$sql4))
                                        {
                                            $i = 1;
                                            // Fetch one and one row
                                            while ($row4=mysqli_fetch_array($result4))
                                            {
                                    ?>
                                        <tr>
                                            <td><?php echo $row4['forms_title']; ?></td>
                                            <td><?php echo $row4['upload_date']; ?></td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#forms<?php echo $i ?>">Delete</button>
                                                <div class="modal fade" id="forms<?php echo $i ?>">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Forum Password</h5>
                                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="needs-validation1" action="../process/delete_files" method="post" novalidate>
                                                                    <input type="hidden" name="type" value="<?php echo base64_encode('forms'); ?>">
                                                                    <input type="hidden" name="dtype" value="<?php echo base64_encode($row4['type']); ?>">
                                                                    <input type="hidden" name="title" value="<?php echo base64_encode($row4['forms_title']); ?>">
                                                                    <p>Sure Detele this Files ?</p>
                                                                    <p><?php echo $row4['forms_title']; ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-xs btn-warning btn-rounded">Delete</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php       $i++;
                                            }
                                        // Free result set                                        
                                        mysqli_free_result($result4);
                                        }
                                    ?>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>