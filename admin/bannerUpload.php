<!DOCTYPE html>
<?php 
session_start();
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Banner Upload</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="needs-validation" action="../process/upload_banner" method="post" enctype="multipart/form-data" novalidate>                                    
                                    <h4 class="mb-5 card-title">Banner Details</h4>
                                    <div class="form-row">   
                                        <div class="col-md-6 mb-6">
                                            <label for="validationCustom03">Banner Title</label>
                                            <input type="text" name="banner_title" class="form-control" id="validationCustom03" placeholder="Banner Title" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="form-row">                 
                                        <div class="col-md-6">
                                            <br/>
                                            <h4 class="card-title mb-5">Banner Drop File (<a class="text-info">Optimal Resolution 900px * 300px</a>)</h4>
                                            <p class="mb-5">Only file type "<a class="text-primary">JPG, JPEG, PNG, jpg, jpeg, png</a>"</p>
                                            <div class="dropify-default">
                                                <input type="file" name="banner" class="dropify" data-allowed-file-extensions="jpg jpeg png JPG JPEG PNG"  />
                                            </div>  
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <br/>
                                            <div class="form-group pl-4">
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                                <label class="form-check-label ml-3" for="invalidCheck">
                                                    Agree to terms and conditions
                                                </label>
                                                <div class="invalid-feedback">
                                                    You must agree before submitting.
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary bs-submit" name="submit" type="submit">Upload Banner</button>
                                    <button class="btn btn-danger" type="button">Cancel</button>        
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>