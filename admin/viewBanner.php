<!DOCTYPE html>
<?php 
session_start();
include("../process/inc_db.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Album / Photo Gallery</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                <?php

                    $sql="SELECT * FROM banner";

                    if ($result=mysqli_query($con,$sql)){
                        // Fetch one and one row
                        while ($row=mysqli_fetch_array($result)){   
                            
                            
                            
                ?>
                    

                    <div class="col-md-6 col-xl-6">
                        <div class="card">
                            <div class="position-relative">
                                <img alt="" src="../<?php echo $row['banner_path']; ?>" class="img-fluid img-full" style="height:286px;">
                                <div class="card-img-btn">
                                    
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-rounded btn-danger btn-xs" data-toggle="modal" data-target="#edit<?php echo $row['id']; ?>">Delete</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="edit<?php echo $row['id']; ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Banner Delete</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <form action="process/delete_banner" method="post">
                                                <input type="hidden" name="banner_title" value="<?php echo $row['banner_title']; ?>">
                                                <input type="hidden" name="type" value="<?php echo $row['type']; ?>">
                                                <div class="modal-body">
                                                    <p> Delete This Banner -<?php echo $row['banner_title']; ?> ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                    <button type="submit" value="edit" class="btn btn-danger btn-xs">Delete</button>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="card-footer py-4 px-xl-5">
                                <p class="card-text d-inline"><span class="text-muted"><?php echo $row['date_upload']; ?></span></p>                                
                            </div>
                        </div>
                    </div>
                <?php
                        }
                        // Free result set
                        mysqli_free_result($result);
                    }
                ?>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>