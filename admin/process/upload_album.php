<?php
include("../../process/inc_db.php");

// $uploaded_images = array();

// foreach ($_FILES['album']['name'] as $key => $val) {
//     $upload_dir = "../../uploads/albums";
//     $upload_file = $upload_dir . $_FILES['album']['name'][$key];
//     $filename = $_FILES['album']['name'][$key];

//     if (move_uploaded_file($_FILES['album']['tmp_name'][$key], $upload_file)) {
//         $uploaded_images[] = $upload_file;
//         $sql = "INSERT INTO album(file_name)
//                         VALUES('$filename')";

//         if (!mysqli_query($con, $sql)) {
//             die('Error: ' . mysqli_error($con));
//         }

//         echo "<script>document.location.href='../albumUpload';</script>";
//     }
// }

//Name of our directory
// $dir_name = 'my_directory';

// //Check if the directory with the name already exists
// if (!is_dir($dir_name)) {
//     //Create our directory if it does not exist
//     mkdir($dir_name);
//     echo "Directory created";
// }

if (isset($_POST['submit'])) {

    $album_title = mysqli_real_escape_string($con, $_POST['album_title']);
    $description = mysqli_real_escape_string($con, $_POST['description']);
    $dir_name = str_replace(' ', '_', mysqli_real_escape_string($con, $_POST['folder_name']));
    
    //Check if the directory with the name already exists
    if (!is_dir("../../uploads/albums/".$dir_name)) {
        //Create our directory if it does not exist
        mkdir("../../uploads/albums/".$dir_name, 0755);

        $uploaded_images = array();

        foreach ($_FILES['album']['name'] as $key => $val) {
            $filename = $_FILES["album"]["name"][$key];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["album"]["size"][$key];
            $allowed_file_types = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG');

            if (in_array($file_ext, $allowed_file_types) && ($filesize < 500000000)) {	
                // Rename file
                $newfilename = $file_basename . $file_ext;
                if (file_exists("../../uploads/albums/".$dir_name."/" . $newfilename)) {
                    // file already exists error
                    echo "<script>alert('You have already uploaded this file !!!');window.history.back();</script>"; 
                } else {

                    $question_path = "uploads/albums/".$dir_name."/" . $newfilename;

                    $sql = "INSERT INTO album(album_title, file_name, album_dir)
                            VALUES('$album_title', '$filename', '$dir_name')";                   

                    if (!mysqli_query($con, $sql)) {
                        die('Error: ' . mysqli_error($con));
                    }else {
                        move_uploaded_file($_FILES["album"]["tmp_name"][$key], "../../uploads/albums/".$dir_name."/" . $newfilename);                        
                    }   
                }
            } elseif (empty($file_basename)) {	
                // file selection error
                echo "<script>alert('Please select a file to upload!!!');window.history.back();</script>"; 
            } elseif ($filesize > 500000000) {	
                // file size error
                echo "<script>alert('The file you are trying to upload is too large!!!');window.history.back();</script>"; 
            } else {
                // file type error
                //echo "Only these file typs are allowed for upload: " . implode(', ', $allowed_file_types);
                echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
                //unlink($_FILES["album"]["tmp_name"][$key]);
            }            
        }
        
        $sql1 = "INSERT INTO album_detail(album_title, description, album_dir)
                VALUES('$album_title', '$description', '$dir_name')";

        if (!mysqli_query($con, $sql1)) {
            die('Error: ' . mysqli_error($con));
        }else {
            echo "<script>alert('Album Added !!!');document.location.href='../viewAlbum';</script>";             
        }
    }else{
        echo "<script>alert('Album already exists !!!');window.history.back();</script>"; 
    }
}
?>