<?php
include("../../process/inc_db.php");

if($_POST['type']=='add'){
    $campus_name = mysqli_real_escape_string($con, $_POST['campus_name']);

    $sql="INSERT INTO campus (campus_name)
            VALUES ('$campus_name')";

    if (!mysqli_query($con,$sql)) {
        die('Error: ' . mysqli_error($con));    
    }

    echo "<script>document.location.href='../campusList';</script>";
}else if($_POST['type']=='edit'){
    $campus_name = mysqli_real_escape_string($con, $_POST['campus_name']);
    $id = mysqli_real_escape_string($con, $_POST['id']);

    $sql = "UPDATE campus SET campus_name='$campus_name' WHERE id='$id'";

    if (mysqli_query($con, $sql)) {
        echo "<script>document.location.href='../campusList';</script>";
    } else {
        echo "<script>alert('Edit Error!!!');document.location.href='../campusList';</script>";
    }
}else if($_POST['type']=='delete'){
    $id = mysqli_real_escape_string($con, $_POST['id']);

    $sql = "DELETE FROM campus WHERE id='$id'";

    if (mysqli_query($con, $sql)) {
        echo "<script>document.location.href='../campusList';</script>";
    } else {
        echo "<script>alert('Delete Error!!!');document.location.href='../campusList';</script>";;
    }
}else{
    echo "<script>alert('Use Form!!!');document.location.href='../home';</script>";
}

mysqli_close($con);
?>
