<?php
// include("../../process/inc_db.php");

$i = 0;

$sql="SELECT * FROM user WHERE status='Inactive'";

if ($result=mysqli_query($con,$sql)){
    // Fetch one and one row
    while ($row=mysqli_fetch_array($result)){

            $sql1="SELECT * FROM student WHERE email='$row[email]'";

            if ($result1=mysqli_query($con,$sql1)){
                // Fetch one and one row
                while ($row1=mysqli_fetch_array($result1)){
?>
                <tr>
                    <td><?php echo $row1['full_name']; ?></td>
                    <td><?php echo $row['email']; ?></td>
                    <td width="450px">
                        <!-- <form action="viewStudent" method="post">
                            <input type="hidden" name="studentEmail" value="<?php //echo $row['email']; ?>">
                            <button type="submit" class="btn btn-xs btn-primary">View Details</button>
                            <a href="#" onclick="document.getElementById('viewStudent').submit();">Submit</a> 
                        </form> -->

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-rounded btn-outline-primary btn-xs" data-toggle="modal" data-target="#detail<?php echo $i; ?>">VIew Details</button>
                        <!-- Modal -->
                        <div class="modal fade" id="detail<?php echo $i; ?>">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Student Details</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Full Name : <?php echo $row1['full_name']; ?>
                                        <br/>
                                        Email : <?php echo $row['email']; ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Button -->
                        <button type="button" class="btn btn-rounded btn-outline-success btn-xs" data-toggle="modal" data-target="#active<?php echo $i; ?>">Active User</button>
                        <!-- Modal -->
                        <div class="modal fade" id="active<?php echo $i; ?>">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Student Details</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Active This User ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                        <form action="process/activeStudent" method="post">
                                            <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                            <button type="submit" class="btn btn-success btn-xs">Confirm</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Confirm Button -->
                        <button type="button" class="btn btn-rounded btn-outline-danger btn-xs" data-toggle="modal" data-target="#ban<?php echo $i; ?>">Ban User</button>
                        <!-- Modal -->
                        <div class="modal fade" id="ban<?php echo $i; ?>">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Student Ban</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Ban This User ?
                                    </div>
                                    <div class="modal-footer">
                                        <form action="process/banStudent" method="post">
                                            <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                            <button type="submit" class="btn btn-warning btn-xs">Ban</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
<?php
                    $i++;
                }
            }
        }
    // Free result set
    mysqli_free_result($result);
}

?>