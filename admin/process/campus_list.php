<?php

$sql="SELECT * FROM campus ORDER BY campus_name DESC";

if ($result=mysqli_query($con,$sql)){
    // Fetch one and one row
    while ($row=mysqli_fetch_array($result)){
?>
            <tr>
                <td><?php echo $row['campus_name']; ?></td>
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-rounded btn-outline-primary btn-xs" data-toggle="modal" data-target="#edit<?php echo $row['id']; ?>">Edit</button>
                    <!-- Modal -->
                    <div class="modal fade" id="edit<?php echo $row['id']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Campus Details</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                    </button>
                                </div>
                                <form action="process/campus" method="post">
                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                <div class="modal-body">
                                    <div class="input-group">
                                        
                                        <input type="text" name="campus_name" class="form-control" placeholder="Campus Name" value="<?php echo $row['campus_name']; ?>" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <button type="submit" value="edit" name="type" class="btn btn-success btn-xs">Save</button>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Confirm Button -->
                    <button type="button" class="btn btn-rounded btn-outline-danger btn-xs" data-toggle="modal" data-target="#delete<?php echo $row['id']; ?>">Delete</button>
                    <!-- Modal -->
                    <div class="modal fade" id="delete<?php echo $row['id']; ?>">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Campus Delete</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Delete This Campus ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                    <form action="process/campus" method="post">
                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                        <button type="submit" value="delete" name="type" class="btn btn-danger btn-xs">Confirm</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
<?php
        }
    // Free result set
    mysqli_free_result($result);
}

?>
