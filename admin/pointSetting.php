<!DOCTYPE html>
<?php 
session_start();
include("../process/setting.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Points Setting</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="needs-validation" action="process/update_point" method="post" novalidate>                                    
                                    <h4 class="mb-5 card-title">Points Setting</h4>
                                    <div class="form-row">   
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom031">Upload Point</label>
                                            <input type="text" name="upload_point" class="form-control" id="validationCustom031" value="<?php echo $upload_point ?>" placeholder="Upload Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>  
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom032">Download Point</label>
                                            <input type="text" name="download_point" class="form-control" id="validationCustom032" value="<?php echo $download_point ?>" placeholder="Download Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom033">Login Point</label>
                                            <input type="text" name="login_point" class="form-control" id="validationCustom033" value="<?php echo $login_point ?>" placeholder="Login Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>    
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom034">Forum Point</label>
                                            <input type="text" name="forum_point" class="form-control" id="validationCustom034" value="<?php echo $forum_point ?>" placeholder="Forum Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">   
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom035">Join Forum Point</label>
                                            <input type="text" name="join_forum_point" class="form-control" id="validationCustom035" value="<?php echo $join_forum_point ?>" placeholder="Join Forum Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>  
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom036">Comment Point</label>
                                            <input type="text" name="comment_point" class="form-control" id="validationCustom036" value="<?php echo $comment_point ?>" placeholder="Comment Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom037">Signup Point</label>
                                            <input type="text" name="signup_point" class="form-control" id="validationCustom037" value="<?php echo $signup_point ?>" placeholder="Signup Point" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>    
                                        
                                    </div>
                                    <br/>
                                    <button class="btn btn-primary bs-submit" name="submit" type="submit">Update Point</button>
                                    <button class="btn btn-danger" type="button">Cancel</button>        
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>