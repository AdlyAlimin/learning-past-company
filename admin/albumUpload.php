<!DOCTYPE html>
<?php 
session_start();
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Album / Photo Upload</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="needs-validation" action="process/upload_album" method="post" enctype="multipart/form-data" novalidate>                                    
                                    <h4 class="mb-5 card-title">Album Details</h4>
                                    <div class="form-row">   
                                        <div class="col-md-6 mb-6">
                                            <label for="validationCustom03">Album Title</label>
                                            <input type="text" name="album_title" class="form-control" id="validationCustom03" placeholder="Album Title" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>    
                                        <div class="col-md-6 mb-6">
                                            <label for="validationCustom03">Folder Name</label>
                                            <input type="text" name="folder_name" class="form-control" id="validationCustom03" placeholder="Folder Name" required>
                                            <div class="invalid-feedback">
                                                Please Fill!
                                            </div>
                                        </div>    
                                        <div class="col-md-12 mb-3">
                                            <label for="description1">Album Description</label>
                                            <textarea name="description" rows="4" placeholder="Please write description here" class="form-control" id="description1" required></textarea>
                                            <div class="invalid-feedback">
                                                Please write description here
                                            </div>
                                        </div>                           
                                        <div class="col-md-6">
                                            <br/>
                                            <h4 class="card-title mb-5">Album Drop File (Select Multiple)</h4>
                                            <p class="mb-5">Only file type "<a class="text-primary">JPG, JPEG, PNG, jpg, jpeg, png</a>"</p>
                                            <div class="dropify-default">
                                                <input type="file" name="album[]" class="dropify" data-allowed-file-extensions="jpg jpeg png JPG JPEG PNG" multiple  />
                                            </div>  
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <br/>
                                            <div class="form-group pl-4">
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                                <label class="form-check-label ml-3" for="invalidCheck">
                                                    Agree to terms and conditions
                                                </label>
                                                <div class="invalid-feedback">
                                                    You must agree before submitting.
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary bs-submit" name="submit" type="submit">Upload Album</button>
                                    <button class="btn btn-danger" type="button">Cancel</button>        
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php include("script.php"); ?>
</body>

</html>