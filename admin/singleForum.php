<!DOCTYPE html>
<?php 
session_start();
include("../process/lect_detail.php");
include("../process/forum_detail.php");

// $sql="SELECT * FROM lecture WHERE email='$_SESSION[email]'";

// if ($result=mysqli_query($con,$sql))
//     {
//     // Fetch one and one row
//     while ($row=mysqli_fetch_array($result))
//         {
//             $moderator_name = $row['full_name'];
//         }
//     // Free result set
//     mysqli_free_result($result);
// }



?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="index"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Forum</li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-5">
                                    <h4 class="card-title card-intro-title">Forum</h4>
                                </div>
                                
                                <!-- Nav tabs -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card story-widget">
                                            <div class="card-header border-bottom d-flex justify-content-between">
                                                <h4 class="heading-primary"><?php echo $forum_title ?></h4>
                                                <p class="font-small"><?php echo date( "M, d, Y g:i A", strtotime($date_create)); ?></p>
                                                <?php

                                                    $sql5="SELECT * FROM participant WHERE student_email='$_SESSION[email]' AND forum_id='$forum_id'";
                                                    $result5=mysqli_query($con,$sql5);

                                                    // Associative array
                                                    $row5=mysqli_fetch_assoc($result5);

                                                    if($row5>0)
                                                    {
                                                ?>
                                                        <a class="btn btn-xs btn-warning btn-rounded" href="javascript:void()" data-toggle="modal" data-target="#exitForum">Exit Forum</a>
                                                <?php                                                   
                                                    }
                                                    else
                                                    {
                                                        
                                                    }                                                   
                                                ?>
                                            </div>
                                            <div class="card-body">
                                                <p><?php echo $description ?></p>
                                                

                                                <?php

                                                $sql="SELECT * FROM participant WHERE student_email='$_SESSION[email]' AND forum_id='$forum_id'";
                                                $result=mysqli_query($con,$sql);

                                                // Associative array
                                                $row=mysqli_fetch_assoc($result);

                                                if($row>0)
                                                {
                                                ?>
                                                <div class="form-group mt-5 mb-0 absolute-icon-append-widget">
                                                    <form action="../process/save_comment_lect" method="post">
                                                        <span class="absolute-form-icon absolute-form-icon-append">
                                                            <i class="fa fa-comment" aria-hidden="true"></i>
                                                        </span>
                                                        <input type="hidden" name="forum_id" value="<?php echo htmlentities($forum_id); ?>">
                                                        <input type="text" name="comment" placeholder="Wtite your comments" name="comment3" id="comment3" class="form-control bg-white">
                                                        <br/>
                                                        <button class="btn btn-xs btn-success btn-rounded" name="submit" type="submit">Place Comment</button>
                                                    </form>
                                                </div>
                                                <?php                                                   
                                                }
                                                else
                                                {
                                                    echo "Please Join The Forum First to Place Comment. Thank You.";
                                                }

                                                // Free result set
                                                mysqli_free_result($result);

                                                mysqli_close($con);
                                                ?>
                                                
                                                
                                                <?php include("../process/student_comment.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>