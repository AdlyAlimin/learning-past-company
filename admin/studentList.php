<!DOCTYPE html>
<?php 
session_start();
include("../process/user_detail.php");
include("../count/student_count.php");
?>
<html lang="en">

<head>
    <?php include("head.php"); ?>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="home"><b><img src="../assets/images/logo1.png" alt=""> </b><span class="brand-title"><img src="../assets/images/logo1-text.png" alt=""></span></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <?php include("head_content.php"); ?>
        <!--**********************************
            Header end
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include("sidebar.php"); ?>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Toward the Knowledge-based Society</h4>
                    </div>
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card flex-row download-docs-card flex-wrap">
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/icons/8.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2><?php echo $active; ?></h2>
                                        <p>Active</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/icons/8.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2 style="color:red;"><?php echo $banned; ?></h2>
                                        <p>Banned</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-right d-flex justify-content-center">
                                <div class="media  document align-items-center">
                                    <img class="mr-4" src="../assets/images/icons/8.png" alt="">
                                    <div class="media-body pl-1">
                                        <h2 style="color:grey;"><?php echo $inactive; ?></h2>
                                        <p>Inactive</p>
                                    </div>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">Student List</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example-advance-1" class="display">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                // include("../../process/inc_db.php");

                                                $sql="SELECT * FROM user";

                                                if ($result=mysqli_query($con,$sql)){
                                                    // Fetch one and one row
                                                    while ($row=mysqli_fetch_array($result)){

                                                            $sql1="SELECT * FROM student WHERE email='$row[email]'";

                                                            if ($result1=mysqli_query($con,$sql1)){
                                                                // Fetch one and one row
                                                                while ($row1=mysqli_fetch_array($result1)){
                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row1['full_name']; ?></td>
                                                                    <td><?php echo $row['email']; ?></td>
                                                                    <?php 
                                                                        if($row['status']=="Active"){
                                                                    ?>
                                                                        <td style="color:green;"><?php echo $row['status']; ?></td>
                                                                        <td width="450px">
                                                                           

                                                                            <!-- Button trigger modal -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-primary btn-xs" data-toggle="modal" data-target="#detail<?php echo $row['username']; ?>">VIew Details</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="detail<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Details</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Full Name : <?php echo $row1['full_name']; ?>
                                                                                            <br/>
                                                                                            Email : <?php echo $row['email']; ?>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!-- Confirm Button -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-danger btn-xs" data-toggle="modal" data-target="#ban<?php echo $row['username']; ?>">Ban User</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="ban<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Ban</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Ban This User ?
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <form action="process/banStudent" method="post">
                                                                                                <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                                                                                <button type="submit" class="btn btn-warning btn-xs">Ban</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    <?php
                                                                        }else if($row['status']=="Inactive"){
                                                                    ?>
                                                                        <td style="color:grey;"><?php echo $row['status']; ?></td>
                                                                        <td width="450px">
                                                                            <!-- <form action="viewStudent" method="post">
                                                                                <input type="hidden" name="studentEmail" value="<?php //echo $row['email']; ?>">
                                                                                <button type="submit" class="btn btn-xs btn-primary">View Details</button>
                                                                                <a href="#" onclick="document.getElementById('viewStudent').submit();">Submit</a> 
                                                                            </form> -->

                                                                            <!-- Button trigger modal -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-primary btn-xs" data-toggle="modal" data-target="#detail<?php echo $row['username']; ?>">VIew Details</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="detail<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Details</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Full Name : <?php echo $row1['full_name']; ?>
                                                                                            <br/>
                                                                                            Email : <?php echo $row['email']; ?>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!-- Confirm Button -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-success btn-xs" data-toggle="modal" data-target="#active<?php echo $row['username']; ?>">Active User</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="active<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Details</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Active This User ?
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                                                            <form action="process/activeStudent" method="post">
                                                                                                <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                                                                                <button type="submit" class="btn btn-success btn-xs">Confirm</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!-- Confirm Button -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-danger btn-xs" data-toggle="modal" data-target="#ban<?php echo $row['username']; ?>">Ban User</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="ban<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Ban</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Ban This User ?
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <form action="process/banStudent" method="post">
                                                                                                <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                                                                                <button type="submit" class="btn btn-warning btn-xs">Ban</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    <?php
                                                                        }else{
                                                                    ?>
                                                                        <td style="color:red;"><?php echo $row['status']; ?></td>
                                                                        <td width="450px">
                                                                            <!-- Button trigger modal -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-primary btn-xs" data-toggle="modal" data-target="#detail<?php echo $row['username']; ?>">VIew Details</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="detail<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Details</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Full Name : <?php echo $row1['full_name']; ?>
                                                                                            <br/>
                                                                                            Email : <?php echo $row['email']; ?>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!-- Confirm Button -->
                                                                            <button type="button" class="btn btn-rounded btn-outline-success btn-xs" data-toggle="modal" data-target="#active<?php echo $row['username']; ?>">Active User</button>
                                                                            <!-- Modal -->
                                                                            <div class="modal fade" id="active<?php echo $row['username']; ?>">
                                                                                <div class="modal-dialog" role="document">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                            <h5 class="modal-title">Student Details</h5>
                                                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                                            </button>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            Active This User ?
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary btn-xs" data-dismiss="modal">Close</button>
                                                                                            <form action="process/activeStudent" method="post">
                                                                                                <input type="hidden" name="email" value="<?php echo $row['email']; ?>">
                                                                                                <button type="submit" class="btn btn-success btn-xs">Confirm</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    <?php
                                                                        }
                                                                    ?>                                                                    
                                                                </tr>
                                                <?php

                                                                }
                                                            }
                                                        }
                                                    // Free result set
                                                    mysqli_free_result($result);
                                                }

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <?php include("footer.php"); ?>
        <!--**********************************
            Footer end
        ***********************************-->

    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <?php
     include("script.php"); 

     mysqli_close($con);
     
     ?>

</body>

</html>