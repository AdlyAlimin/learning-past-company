<!DOCTYPE html>
<html lang="en" class="h-100" id="login-page1">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>IS4L - Register</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="assets/css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <div class="login-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content login-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="logo text-center">
                                    <a href="index">
                                        <img src="assets/images/logo1.png" alt="">
                                    </a>
                                </div>
                                <h4 class="text-center mt-4">Register as Lecture / Staff</h4>
                                <form class="mt-5 mb-5" action="process/save_registerlect" method="post" autocomplete="off">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-3">Select Role</label>
                                        <div class="col-lg-9">
                                            <select class="form-control" name="type" required>
                                                <option class="text-muted" disabled selected style="display: none" value="">-- SELECT --</option>
                                                <option value="Lecturer">Lecturer</option>
                                                <option value="Student">Lecturer</option>
                                                <option value="Staff">Staff</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Fullname</label>
                                        <input type="text" name="full_name" class="form-control" placeholder="Fullname" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Retype-Password</label>
                                        <input type="password" name="password1" class="form-control" placeholder="Retype-Password" autocomplete="off" required>
                                    </div>
                                    <div class="text-center mb-4 mt-4">
                                        <button type="submit" class="btn btn-success">Register</button>
                                    </div>
                                </form>
                                <div class="text-center">
                                    <p class="mt-5">Already have an account? <a href="index">Login Now</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
    <!-- Common JS -->
    <script src="assets/plugins/common/common.min.js"></script>
    <!-- Custom script -->
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/settings.js"></script>
    <script src="assets/js/gleek.js"></script>
</body>

</html>