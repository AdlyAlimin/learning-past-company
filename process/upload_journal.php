<?php
session_start();
include("inc_db.php");
include("current_point.php");
include("setting.php");

//role path
$role = mysqli_real_escape_string($con, $_SESSION['role']);

// Upload and Rename File
if (isset($_POST['submit']))
{
	$journal_title = mysqli_real_escape_string($con, $_POST['journal_title']);
	$category = mysqli_real_escape_string($con, $_POST['category']);

	$sql4 = "SELECT * FROM journal_point WHERE journalp_email='$_SESSION[email]'";
	$result4 = mysqli_query($con, $sql4);

	if (mysqli_num_rows($result4) > 0) {

		// output data of each row
		while($row4 = mysqli_fetch_assoc($result4)) {

			$start_date = date('Y-m-d H:i:s');
			$end_date = date("Y-m-d H:i:s", strtotime('+24 hours'));

			$sql2="SELECT count(*) AS point FROM journal WHERE owner_email='$_SESSION[email]' AND upload_date>='$start_date' AND upload_date<='$end_date'";

			if($result2=mysqli_query($con,$sql2)){
				// Fetch one and one row
				while($row2=mysqli_fetch_array($result2)){
						$current_count = $row2['point'];

						if($current_count>=3){

							$filename = $_FILES["journal"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["journal"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $journal_title . $file_ext;
								if (file_exists("../uploads/journal/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$journal_path = "uploads/journal/".$newfilename;

									$sql="INSERT INTO journal (owner_email, journal_title, journal_path, type, category)
											VALUES ('$_SESSION[email]', '$journal_title', '$journal_path', '$file_ext', '$category')";

									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
									}else {
										move_uploaded_file($_FILES["journal"]["tmp_name"], "../uploads/journal/" . $newfilename);
										echo "<script>alert('Journal Submited.');document.location.href='../".$role."';</script>"; 
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["journal"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}else{

							$filename = $_FILES["journal"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["journal"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $journal_title . $file_ext;
								if (file_exists("../uploads/journal/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$journal_path = "uploads/journal/".$newfilename;

									$new1 = $row4['current_point'] + $upload_point;

									$new2 = $current_point + $upload_point;

									$sql = "INSERT INTO journal (owner_email, journal_title, journal_path, type, category)
											VALUES ('$_SESSION[email]', '$journal_title', '$journal_path', '$file_ext', '$category')";

									$sql2 = "UPDATE journal_point SET current_point='$new1' WHERE journalp_email='$_SESSION[email]'";

									$sql3 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";

									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql2)) {
										die('Error: ' . mysqli_error($con));
										
									}

									if (!mysqli_query($con,$sql3)) {
										die('Error: ' . mysqli_error($con));
										
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
										
									}else {
										move_uploaded_file($_FILES["journal"]["tmp_name"], "../uploads/journal/" . $newfilename);
										echo "<script>alert('Journal Submited.');document.location.href='../".$role."';</script>"; 
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["journal"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}				
					}
				// Free result set
				mysqli_free_result($result2);
			}
			else{
				echo("Error description: " . mysqli_error($con));
			}

			// $journal_title = mysqli_real_escape_string($con, $_POST['journal_title']);
			// $filename = $_FILES["journal"]["name"];
			// $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
			// $file_ext = substr($filename, strripos($filename, '.')); // get file name
			// $filesize = $_FILES["journal"]["size"];
			// $allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

			// if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
			// {	
			// 	// Rename file
			// 	$newfilename = $journal_title . $file_ext;
			// 	if (file_exists("../uploads/journal/" . $newfilename))
			// 	{
			// 		// file already exists error
			// 		echo "You have already uploaded this file.";
			// 	}
			// 	else
			// 	{		
			// 		$journal_path = "uploads/journal/".$newfilename;

			// 		$sql="INSERT INTO journal (owner_email, journal_title, journal_path, type)
			// 				VALUES ('$_SESSION[email]', '$journal_title', '$journal_path', '$file_ext')";
							
			// 		if (!mysqli_query($con,$sql)) {
			// 			die('Error: ' . mysqli_error($con));
			// 		} else {
			// 			move_uploaded_file($_FILES["journal"]["tmp_name"], "../uploads/journal/" . $newfilename);
			// 			echo "File uploaded successfully.";	
			// 		}
			// 	}
			// }
			// elseif (empty($file_basename))
			// {	
			// 	// file selection error
			// 	echo "Please select a file to upload.";
			// } 
			// elseif ($filesize > 500000000)
			// {	
			// 	// file size error
			// 	echo "The file you are trying to upload is too large.";
			// }
			// else
			// {
			// 	// file type error
			// 	echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
			// 	unlink($_FILES["journal"]["tmp_name"]);
			// }
		}
	}else{

		$filename = $_FILES["journal"]["name"];
		$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
		$file_ext = substr($filename, strripos($filename, '.')); // get file name
		$filesize = $_FILES["journal"]["size"];
		$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

		if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
		{	
			// Rename file
			$newfilename = $journal_title . $file_ext;
			if (file_exists("../uploads/journal/" . $newfilename))
			{
				// file already exists error
				echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
			}
			else
			{		
				$journal_path = "uploads/journal/".$newfilename;

				$new2 = $current_point + $upload_point;

				$sql="INSERT INTO journal (owner_email, journal_title, journal_path, type, category)
						VALUES ('$_SESSION[email]', '$journal_title', '$journal_path', '$file_ext', '$category')";

				$sql1="INSERT INTO journal_point (journalp_email, current_point)
						VALUES ('$_SESSION[email]', '$upload_point')";
				
				$sql2 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";

				$sql3 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
						
				if (!mysqli_query($con,$sql)) {
					die('Error: ' . mysqli_error($con));
				} 
				
				if (!mysqli_query($con,$sql1)) {
					die('Error: ' . mysqli_error($con));
				}

				if (!mysqli_query($con,$sql2)) {
					die('Error: ' . mysqli_error($con));
				}

				if (!mysqli_query($con,$sql3)) {
					die('Error: ' . mysqli_error($con));
				}else {
					move_uploaded_file($_FILES["journal"]["tmp_name"], "../uploads/journal/" . $newfilename);
					echo "<script>alert('Journal Submited.');document.location.href='../".$role."';</script>"; 
				}
			}
		}
		elseif (empty($file_basename))
		{	
			// file selection error
			echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
		} 
		elseif ($filesize > 500000000)
		{	
			// file size error
			echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
		}
		else
		{
			// file type error
			// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);			
			// unlink($_FILES["journal"]["tmp_name"]);
			echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
		}
	}
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>