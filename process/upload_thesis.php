<?php
session_start();
include("inc_db.php");
include("current_point.php");
include("setting.php");

//role path
$role = mysqli_real_escape_string($con, $_SESSION['role']);

// Upload and Rename File
if (isset($_POST['submit']))
{
	$thesis_title = mysqli_real_escape_string($con, $_POST['thesis_title']);
	$category = mysqli_real_escape_string($con, $_POST['category']);

	$sql4 = "SELECT * FROM thesis_point WHERE thesisp_email='$_SESSION[email]'";
	$result4 = mysqli_query($con, $sql4);

	if (mysqli_num_rows($result4) > 0) {

		// output data of each row
		while($row4 = mysqli_fetch_assoc($result4)) {

			$start_date = date('Y-m-d H:i:s');
			$end_date = date("Y-m-d H:i:s", strtotime('+24 hours'));

			$sql2="SELECT count(*) AS point FROM thesis WHERE owner_email='$_SESSION[email]' AND upload_date>='$start_date' AND upload_date<='$end_date'";

			if($result2=mysqli_query($con,$sql2)){
				// Fetch one and one row
				while($row2=mysqli_fetch_array($result2)){
						$current_count = $row2['point'];

						if($current_count>=3){

							$filename = $_FILES["thesis"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["thesis"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $thesis_title . $file_ext;
								if (file_exists("../uploads/thesis/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$thesis_path = "uploads/thesis/".$newfilename;

									$sql="INSERT INTO thesis (owner_email, thesis_title, thesis_path, type, category)
											VALUES ('$_SESSION[email]', '$thesis_title', '$thesis_path', '$file_ext', '$category')";

									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
									}else {
										move_uploaded_file($_FILES["thesis"]["tmp_name"], "../uploads/thesis/" . $newfilename);
										echo "<script>alert('Thesis Submited.');document.location.href='../".$role."';</script>"; 
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["thesis"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}else{

							$filename = $_FILES["thesis"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["thesis"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $thesis_title . $file_ext;
								if (file_exists("../uploads/thesis/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$thesis_path = "uploads/thesis/".$newfilename;

									$new1 = $row4['current_point'] + $upload_point;

									$new2 = $current_point + $upload_point;

									$sql="INSERT INTO thesis (owner_email, thesis_title, thesis_path, type, category)
											VALUES ('$_SESSION[email]', '$thesis_title', '$thesis_path', '$file_ext', '$category')";

									$sql2 = "UPDATE thesis_point SET current_point='$new1' WHERE thesisp_email='$_SESSION[email]'";

									$sql3 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";

									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql2)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql3)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
									}else {
										move_uploaded_file($_FILES["thesis"]["tmp_name"], "../uploads/thesis/" . $newfilename);
										echo "<script>alert('Thesis Submited.');document.location.href='../".$role."';</script>";  
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["thesis"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}				
					}
				// Free result set
				mysqli_free_result($result2);
			}
			else{
				echo("Error description: " . mysqli_error($con));
			}

			// $filename = $_FILES["thesis"]["name"];
			// $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
			// $file_ext = substr($filename, strripos($filename, '.')); // get file name
			// $filesize = $_FILES["thesis"]["size"];
			// $allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

			// if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
			// {	
			// 	// Rename file
			// 	$newfilename = $thesis_title . $file_ext;
			// 	if (file_exists("../uploads/thesis/" . $newfilename))
			// 	{
			// 		// file already exists error
			// 		echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
			// 	}
			// 	else
			// 	{		
			// 		$thesis_path = "uploads/thesis/".$newfilename;

			// 		$sql="INSERT INTO thesis (owner_email, thesis_title, thesis_path, type)
			// 				VALUES ('$_SESSION[email]', '$thesis_title', '$thesis_path', '$file_ext')";
							
			// 		if (!mysqli_query($con,$sql)) {
			// 			die('Error: ' . mysqli_error($con));
			// 		} else {
			// 			move_uploaded_file($_FILES["thesis"]["tmp_name"], "../uploads/thesis/" . $newfilename);
			// 			echo "<script>alert('Comment Submited.');document.location.href='../student';</script>"; 
			// 		}
			// 	}
			// }
			// elseif (empty($file_basename))
			// {	
			// 	// file selection error
			// 	echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
			// } 
			// elseif ($filesize > 500000000)
			// {	
			// 	// file size error
			// 	echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
			// }
			// else
			// {
			// 	// file type error
			// 	echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
			// 	unlink($_FILES["thesis"]["tmp_name"]);
			// }
		}
	}else{

		$filename = $_FILES["thesis"]["name"];
		$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
		$file_ext = substr($filename, strripos($filename, '.')); // get file name
		$filesize = $_FILES["thesis"]["size"];
		$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

		if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
		{	
			// Rename file
			$newfilename = $thesis_title . $file_ext;
			if (file_exists("../uploads/thesis/" . $newfilename))
			{
				// file already exists error
				echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
			}
			else
			{		
				$thesis_path = "uploads/thesis/".$newfilename;

				$new2 = $current_point + $upload_point;

				$sql="INSERT INTO thesis (owner_email, thesis_title, thesis_path, type, category)
						VALUES ('$_SESSION[email]', '$thesis_title', '$thesis_path', '$file_ext', '$category')";

				$sql1="INSERT INTO thesis_point (thesisp_email, current_point)
						VALUES ('$_SESSION[email]', '$upload_point')";

				$sql2 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";

				$sql3 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
						
				if (!mysqli_query($con,$sql)) {
					die('Error: ' . mysqli_error($con));
				} 
				
				if (!mysqli_query($con,$sql1)) {
					die('Error: ' . mysqli_error($con));
				}
				
				if (!mysqli_query($con,$sql2)) {
					die('Error: ' . mysqli_error($con));
				}

				if (!mysqli_query($con,$sql3)) {
					die('Error: ' . mysqli_error($con));
				}else {
					move_uploaded_file($_FILES["thesis"]["tmp_name"], "../uploads/thesis/" . $newfilename);
					echo "<script>alert('Thesis Submited.');document.location.href='../".$role."';</script>";  
				}
			}
		}
		elseif (empty($file_basename))
		{	
			// file selection error
			echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
		} 
		elseif ($filesize > 500000000)
		{	
			// file size error
			echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
		}
		else
		{
			// file type error
			// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
			// unlink($_FILES["thesis"]["tmp_name"]);
			echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
		}
	}
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>