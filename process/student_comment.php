<?php
include("inc_db.php");

$sql="SELECT * FROM comment WHERE comment.forum_id='$forum_id'";

if ($result=mysqli_query($con,$sql))
    {
        $i = 1;
        // Fetch one and one row
        while ($row=mysqli_fetch_array($result))
        {
?>
            <div class="user-comments border-top mt-5 pt-5">
                <div class="media">
                    <img class="mr-3 rounded-circle" src="../assets/images/avatar/50.png" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 text-primary"><?php echo $row['full_name']; ?></h5>
                        <p><?php echo $row['comment']; ?>.</p>
                    </div>

                    <?php
                        if($row['comment_email']==$_SESSION['email']){
                    ?>
                            <div class="ticket-widget__heading--right d-flex">
                                <p><?php echo date( "M, d, Y g:i A", strtotime($row['date_comment'])); ?></p>
                                <div class="dropdown ml-3">
                                    <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="javascript:void()" data-toggle="modal" data-target="#edit<?php echo $i ?>">Edit</a>                                
                                        <a class="dropdown-item" href="javascript:void()" data-toggle="modal" data-target="#delete<?php echo $i ?>">Delete</a>
                                    </div>
                                </div>
                            </div>
                    <?php
                        }else{
                    ?>
                            <div class="ticket-widget__heading--right d-flex">
                                <p><?php echo date( "M, d, Y g:i A", strtotime($row['date_comment'])); ?></p>                                
                            </div>
                    <?php
                        }
                    ?>
                    
                </div>
            </div> 

            <div class="modal fade" id="edit<?php echo $i ?>">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Comment</h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="needs-validation1" action="../process/comment" method="post" novalidate>
                            <input type="hidden" name="forum_id" value="<?php echo $forum_id ?>">
                            <input type="hidden" name="date" value="<?php echo $row['date_comment'] ?>">
                            <div class="form-group">
                                <label class="text-label">Comment</label>
                                <div class="md-form">
                                    <textarea class="form-control rounded-0" rows="5" name="comment" ><?php echo $row['comment']; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-xs btn-danger btn-rounded" data-dismiss="modal">Close</button>
                            <button type="submit" name="submit" value="edit" class="btn btn-xs btn-info btn-rounded">Save changes</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="delete<?php echo $i ?>">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete Comment</h5>
                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="needs-validation1" action="../process/comment" method="post" novalidate>
                            <p>Delete This Comment ?</p>
                            <input type="hidden" name="forum_id" value="<?php echo $forum_id ?>">
                            <input type="hidden" name="date" value="<?php echo $row['date_comment'] ?>">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="submit" value="delete" class="btn btn-xs btn-danger btn-rounded">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<?php       $i++;
        }
    // Free result set
    mysqli_free_result($result);
}

?>
