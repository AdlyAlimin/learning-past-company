<?php
session_start();
include("inc_db.php");
include("current_point.php");
include("setting.php");

if($_POST['type']=='public'){
  // escape variables for security
  $moderator_email = mysqli_real_escape_string($con, $_POST['moderator_email']);
  $forum_title = mysqli_real_escape_string($con, $_POST['forum_title']);
  $forum_category = mysqli_real_escape_string($con, $_POST['forum_category']);
  $number_participant = mysqli_real_escape_string($con, $_POST['number_participant']);
  $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
  $description = mysqli_real_escape_string($con, $_POST['description']);

  $new = $current_point + $forum_point;

  $sql = "INSERT INTO forum (moderator_email, forum_title, forum_category, number_participant, forum_id, description, forum_type)
        VALUES ('$moderator_email', '$forum_title', '$forum_category', '$number_participant' , '$forum_id', '$description', 'Public')";
  
  $sql2 = "UPDATE current_point SET current_point='$new' WHERE point_email='$moderator_email'";

  if (!mysqli_query($con,$sql)) {
    die('Error: ' . mysqli_error($con));
  }

  if (!mysqli_query($con,$sql2)) {
    die('Error: ' . mysqli_error($con));
  }

  echo "<script>alert('Public Forum added successfull.');document.location.href='../lect/addForum';</script>"; 

} else if($_POST['type']=='private'){
  // escape variables for security
  $moderator_email = mysqli_real_escape_string($con, $_POST['moderator_email']);
  $forum_title = mysqli_real_escape_string($con, $_POST['forum_title']);
  $forum_category = mysqli_real_escape_string($con, $_POST['forum_category']);
  $number_participant = mysqli_real_escape_string($con, $_POST['number_participant']);
  $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
  $description = mysqli_real_escape_string($con, $_POST['description']);
  $password = mysqli_real_escape_string($con, $_POST['password']);
  $password1 = mysqli_real_escape_string($con, $_POST['password1']);

  $new = $current_point + $forum_point;

  if($password==$password1){
    $sql="INSERT INTO forum (moderator_email, forum_title, forum_category, number_participant, forum_id, description, forum_type, password)
        VALUES ('$moderator_email', '$forum_title', '$forum_category', '$number_participant' , '$forum_id', '$description', 'Private', '$password')";

    $sql2 = "UPDATE current_point SET current_point='$new' WHERE point_email='$moderator_email'";
  }
  else{
    echo "<script>alert('Password Not Match.');document.location.href='../lect/addForum';</script>"; 
  }

  if (!mysqli_query($con,$sql)) {
    die('Error: ' . mysqli_error($con));
  }

  if (!mysqli_query($con,$sql2)) {
    die('Error: ' . mysqli_error($con));
  }

  echo "<script>alert('Private Forum added successfull.');document.location.href='../lect/addForum';</script>"; 

} else{
  echo "<script>alert('Please Use Submit Button.');document.location.href='../lect/addForum';</script>";
}
// // escape variables for security
// $moderator_email = mysqli_real_escape_string($con, $_POST['moderator_email']);
// $forum_title = mysqli_real_escape_string($con, $_POST['forum_title']);
// $forum_category = mysqli_real_escape_string($con, $_POST['forum_category']);
// $number_participant = mysqli_real_escape_string($con, $_POST['number_participant']);
// $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
// $description = mysqli_real_escape_string($con, $_POST['description']);
// $password = mysqli_real_escape_string($con, $_POST['password']);
// $password1 = mysqli_real_escape_string($con, $_POST['password1']);

// if($password==$password1){
//     $sql="INSERT INTO user (username, email, password, role)
//             VALUES ('$username', '$email', '$password', 'Lecture')";
            
//     $sql1="INSERT INTO lecture (email, full_name)
// 	        VALUES ('$email', '$full_name')";
// }

// if (!mysqli_query($con,$sql)) {
//   die('Error: ' . mysqli_error($con));
// }

// if (!mysqli_query($con,$sql1)) {
//     die('Error: ' . mysqli_error($con));
//   }

// echo "<script>alert('Register Successful. Please Login and Complete all the field. Thank You.');document.location.href='../index';</script>"; 

 mysqli_close($con);
?>