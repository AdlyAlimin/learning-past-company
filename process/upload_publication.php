<?php
session_start();
include("inc_db.php");
include("current_point.php");
include("setting.php");

//role path
$role = mysqli_real_escape_string($con, $_SESSION['role']);

// Upload and Rename File
if (isset($_POST['submit']))
{
	$publication_title = mysqli_real_escape_string($con, $_POST['publication_title']);
	$campus = mysqli_real_escape_string($con, $_POST['campus']);;

    $filename = $_FILES["publication"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["publication"]["size"];
    $allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	
        // Rename file
        $newfilename = $publication_title . $file_ext;
        if (file_exists("../uploads/publication/" . $newfilename))
        {
            // file already exists error
            echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
        }
        else
        {		
            $publication_path = "uploads/publication/".$newfilename;


            $sql="INSERT INTO publication (owner_email, publication_title, publication_path, type, campus)
                    VALUES ('$_SESSION[email]', '$publication_title', '$publication_path', '$file_ext', '$campus')";

            $sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
                    
            if (!mysqli_query($con,$sql)) {
                die('Error: ' . mysqli_error($con));
            }
            
            if (!mysqli_query($con,$sql5)) {
                die('Error: ' . mysqli_error($con));
            }else {
                move_uploaded_file($_FILES["publication"]["tmp_name"], "../uploads/publication/" . $newfilename);
                echo "<script>alert('Publication Submited.');document.location.href='../".$role."';</script>"; 
            }
        }
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);			
        // unlink($_FILES["publication"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>