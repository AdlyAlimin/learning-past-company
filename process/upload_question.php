<?php
session_start();
include("inc_db.php");
include("current_point.php");
include("setting.php");

//role path
$role = mysqli_real_escape_string($con, $_SESSION['role']);

// Upload and Rename File
if (isset($_POST['submit']))
{
	$question_title = mysqli_real_escape_string($con, $_POST['question_title']);
	$category = mysqli_real_escape_string($con, $_POST['category']);

	$sql4 = "SELECT * FROM question_point WHERE questionp_email='$_SESSION[email]'";
	$result4 = mysqli_query($con, $sql4);

	if (mysqli_num_rows($result4) > 0) {

		// output data of each row
		while($row4 = mysqli_fetch_assoc($result4)) {
 
			$start_date = date('Y-m-d H:i:s');
			$end_date = date("Y-m-d H:i:s", strtotime('+24 hours'));

			$sql2="SELECT count(*) AS point FROM question WHERE owner_email='$_SESSION[email]' AND upload_date>='$start_date' AND upload_date<='$end_date'";

			if($result2=mysqli_query($con,$sql2)){
				// Fetch one and one row
				while($row2=mysqli_fetch_array($result2)){
						$current_count = $row2['point'];

						if($current_count>=3){

							$filename = $_FILES["question"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["question"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $question_title . $file_ext;
								if (file_exists("../uploads/question/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$question_path = "uploads/question/".$newfilename;

									$sql="INSERT INTO question (owner_email, question_title, question_path, type, category)
											VALUES ('$_SESSION[email]', '$question_title', '$question_path', '$file_ext', '$category')";

									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
									}else {
										move_uploaded_file($_FILES["question"]["tmp_name"], "../uploads/question/" . $newfilename);
										echo "<script>alert('Question Submited.');document.location.href='../".$role."';</script>"; 
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["question"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}else{

							$filename = $_FILES["question"]["name"];
							$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
							$file_ext = substr($filename, strripos($filename, '.')); // get file name
							$filesize = $_FILES["question"]["size"];
							$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

							if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
							{	
								// Rename file
								$newfilename = $question_title . $file_ext;
								if (file_exists("../uploads/question/" . $newfilename))
								{
									// file already exists error
									echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
								}
								else
								{		
									$question_path = "uploads/question/".$newfilename;

                                    $new1 = $row4['current_point'] + $upload_point;
                                    
                                    $new2 = $current_point + $upload_point;

									$sql="INSERT INTO question (owner_email, question_title, question_path, type, category)
											VALUES ('$_SESSION[email]', '$question_title', '$question_path', '$file_ext', '$category')";

                                    $sql2 = "UPDATE question_point SET current_point='$new1' WHERE questionp_email='$_SESSION[email]'";
                                    
									$sql3 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";
									
									$sql5 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
											
									if (!mysqli_query($con,$sql)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql2)) {
										die('Error: ' . mysqli_error($con));
                                    }
                                    
                                    if (!mysqli_query($con,$sql3)) {
										die('Error: ' . mysqli_error($con));
									}
									
									if (!mysqli_query($con,$sql5)) {
										die('Error: ' . mysqli_error($con));
									}else {
										move_uploaded_file($_FILES["question"]["tmp_name"], "../uploads/question/" . $newfilename);
										echo "<script>alert('Question Submited.');document.location.href='../".$role."';</script>";  
									}
								}
							}
							elseif (empty($file_basename))
							{	
								// file selection error
								echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
							} 
							elseif ($filesize > 500000000)
							{	
								// file size error
								echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
							}
							else
							{
								// file type error
								// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
								// unlink($_FILES["question"]["tmp_name"]);
								echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
							}
						}				
					}
				// Free result set
				mysqli_free_result($result2);
			}
			else{
				echo("Error description: " . mysqli_error($con));
			}

			// $filename = $_FILES["question"]["name"];
			// $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
			// $file_ext = substr($filename, strripos($filename, '.')); // get file name
			// $filesize = $_FILES["question"]["size"];
			// $allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

			// if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
			// {	
			// 	// Rename file
			// 	$newfilename = $question_title . $file_ext;
			// 	if (file_exists("../uploads/question/" . $newfilename))
			// 	{
			// 		// file already exists error
			// 		echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
			// 	}
			// 	else
			// 	{		
			// 		$question_path = "uploads/question/".$newfilename;

			// 		$sql="INSERT INTO question (owner_email, question_title, question_path, type)
			// 				VALUES ('$_SESSION[email]', '$question_title', '$question_path', '$file_ext')";
							
			// 		if (!mysqli_query($con,$sql)) {
			// 			die('Error: ' . mysqli_error($con));
			// 		} else {
			// 			move_uploaded_file($_FILES["question"]["tmp_name"], "../uploads/question/" . $newfilename);
			// 			echo "<script>alert('Comment Submited.');document.location.href='../student';</script>"; 
			// 		}
			// 	}
			// }
			// elseif (empty($file_basename))
			// {	
			// 	// file selection error
			// 	echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
			// } 
			// elseif ($filesize > 500000000)
			// {	
			// 	// file size error
			// 	echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
			// }
			// else
			// {
			// 	// file type error
			// 	echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
			// 	unlink($_FILES["question"]["tmp_name"]);
			// }
		}
	}else{

		$filename = $_FILES["question"]["name"];
		$file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
		$file_ext = substr($filename, strripos($filename, '.')); // get file name
		$filesize = $_FILES["question"]["size"];
		$allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

		if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
		{	
			// Rename file
			$newfilename = $question_title . $file_ext;
			if (file_exists("../uploads/question/" . $newfilename))
			{
				// file already exists error
				echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
			}
			else
			{		
                $question_path = "uploads/question/".$newfilename;
                
                $new2 = $current_point + $upload_point;

				$sql="INSERT INTO question (owner_email, question_title, question_path, type, category)
						VALUES ('$_SESSION[email]', '$question_title', '$question_path', '$file_ext', '$category')";

				$sql1="INSERT INTO question_point (questionp_email, current_point)
                        VALUES ('$_SESSION[email]', '$upload_point')";
                        
				$sql2 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$_SESSION[email]'";
				
				$sql3 = "UPDATE user SET upload_status='$download_limit' WHERE email='$_SESSION[email]'";
						
				if (!mysqli_query($con,$sql)) {
					die('Error: ' . mysqli_error($con));
				} 
				
				if (!mysqli_query($con,$sql1)) {
					die('Error: ' . mysqli_error($con));
                }
                
                if (!mysqli_query($con,$sql2)) {
					die('Error: ' . mysqli_error($con));
				}

				if (!mysqli_query($con,$sql3)) {
					die('Error: ' . mysqli_error($con));
				}else {
					move_uploaded_file($_FILES["question"]["tmp_name"], "../uploads/question/" . $newfilename);
					echo "<script>alert('Question Submited.');document.location.href='../".$role."';</script>";  
				}
			}
		}
		elseif (empty($file_basename))
		{	
			// file selection error
			echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
		} 
		elseif ($filesize > 500000000)
		{	
			// file size error
			echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
		}
		else
		{
			// file type error
			// echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
			// unlink($_FILES["question"]["tmp_name"]);
			echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
		}
	}
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>