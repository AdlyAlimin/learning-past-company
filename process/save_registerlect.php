<?php
include("inc_db.php");
include("setting.php");

$type = mysqli_real_escape_string($con, $_POST['type']);

if ($type == "Lecturer") {
  $email = mysqli_real_escape_string($con, $_POST['email']);
  $password = mysqli_real_escape_string($con, $_POST['password']);
  $password1 = mysqli_real_escape_string($con, $_POST['password1']);
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $full_name = mysqli_real_escape_string($con, $_POST['full_name']);

  if ($password == $password1) {

    $sql = "INSERT INTO user (username, email, password, role)
          VALUES ('$username', '$email', '$password', 'Lecture')";

    if (!mysqli_query($con, $sql)) {
      //die('Error: ' . mysqli_error($con));
      echo "<script>alert('Register Error. Please Try Again.');window.history.back();</script>";
    } else {

      $sql1 = "INSERT INTO lecture (email, full_name)
            VALUES ('$email', '$full_name')";

      if (!mysqli_query($con, $sql1)) {
        die('Error: ' . mysqli_error($con));
      } else {

        $sql2 = "INSERT INTO thesis_point (thesisp_email)
              VALUES ('$email')";

        if (!mysqli_query($con, $sql2)) {
          die('Error: ' . mysqli_error($con));
        } else {

          $sql3 = "INSERT INTO journal_point (journalp_email)
                VALUES ('$email')";

          if (!mysqli_query($con, $sql3)) {
            die('Error: ' . mysqli_error($con));
          } else {

            $sql4 = "INSERT INTO comment_point (commentp_email)
                  VALUES ('$email')";

            if (!mysqli_query($con, $sql4)) {
              die('Error: ' . mysqli_error($con));
            } else {

              $sql5 = "INSERT INTO current_point (point_email, current_point)
                    VALUES ('$email', '$signup_point')";

              if (!mysqli_query($con, $sql5)) {
                die('Error: ' . mysqli_error($con));
              } else {
                echo "<script>alert('Register Successful. Please Login and Complete all the field. Thank You.');document.location.href='../index';</script>";
              }
            }
          }
        }
      }
    }
  } else {
    echo "<script>alert('Password Not Match');window.history.back();</script>";
  }
} else if ($type == "Staff") {
  $email = mysqli_real_escape_string($con, $_POST['email']);
  $password = mysqli_real_escape_string($con, $_POST['password']);
  $password1 = mysqli_real_escape_string($con, $_POST['password1']);
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $full_name = mysqli_real_escape_string($con, $_POST['full_name']);

  if ($password == $password1) {

    $sql = "INSERT INTO user (username, email, password, role)
          VALUES ('$username', '$email', '$password', 'Staff')";

    if (!mysqli_query($con, $sql)) {
      //die('Error: ' . mysqli_error($con));
      echo "<script>alert('Register Error. Please Try Again.');window.history.back();</script>";
    } else {

      $sql1 = "INSERT INTO staff (email, full_name)
            VALUES ('$email', '$full_name')";

      if (!mysqli_query($con, $sql1)) {
        die('Error: ' . mysqli_error($con));
      } else {

        $sql2 = "INSERT INTO thesis_point (thesisp_email)
              VALUES ('$email')";

        if (!mysqli_query($con, $sql2)) {
          die('Error: ' . mysqli_error($con));
        } else {

          $sql3 = "INSERT INTO journal_point (journalp_email)
                VALUES ('$email')";

          if (!mysqli_query($con, $sql3)) {
            die('Error: ' . mysqli_error($con));
          } else {
            
            $sql5 = "INSERT INTO current_point (point_email, current_point)
                  VALUES ('$email', '$signup_point')";

            if (!mysqli_query($con, $sql5)) {
              die('Error: ' . mysqli_error($con));
            } else {
              echo "<script>alert('Register Successful. Please Login and Complete all the field. Thank You.');document.location.href='../index';</script>";
            }
          }
        }
      }
    }
  } else {
    echo "<script>alert('Password Not Match');window.history.back();</script>";
  }
} else if ($type == "Student") {
  $email = mysqli_real_escape_string($con, $_POST['email']);
  $password = mysqli_real_escape_string($con, $_POST['password']);
  $password1 = mysqli_real_escape_string($con, $_POST['password1']);
  $username = mysqli_real_escape_string($con, $_POST['username']);
  $full_name = mysqli_real_escape_string($con, $_POST['full_name']);

  if ($password == $password1) {

    $sql = "INSERT INTO user (username, email, password, role)
          VALUES ('$username', '$email', '$password', 'Student')";

    if (!mysqli_query($con, $sql)) {
      //die('Error: ' . mysqli_error($con));
      echo "<script>alert('Register Error. Please Try Again.');window.history.back();</script>";
    } else {

      $sql1 = "INSERT INTO student (email, full_name)
            VALUES ('$email', '$full_name')";

      if (!mysqli_query($con, $sql1)) {
        die('Error: ' . mysqli_error($con));
      } else {

        $sql2 = "INSERT INTO thesis_point (thesisp_email)
              VALUES ('$email')";

        if (!mysqli_query($con, $sql2)) {
          die('Error: ' . mysqli_error($con));
        } else {

          $sql3 = "INSERT INTO journal_point (journalp_email)
                VALUES ('$email')";

          if (!mysqli_query($con, $sql3)) {
            die('Error: ' . mysqli_error($con));
          } else {
            
            $sql5 = "INSERT INTO current_point (point_email, current_point)
                  VALUES ('$email', '$signup_point')";

            if (!mysqli_query($con, $sql5)) {
              die('Error: ' . mysqli_error($con));
            } else {
              echo "<script>alert('Register Successful. Please Login and Complete all the field. Thank You.');document.location.href='../index';</script>";
            }
          }
        }
      }
    }
  } else {
    echo "<script>alert('Password Not Match');window.history.back();</script>";
  }
} else {
  echo "<script>alert('Please Select Role');window.history.back();</script>";
}

mysqli_close($con);
?>