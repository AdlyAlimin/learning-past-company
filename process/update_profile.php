<?php
session_start();
include("inc_db.php");

if($_POST['type']=='student'){
    $email = mysqli_real_escape_string($con, $_SESSION['email']);
    $username = mysqli_real_escape_string($con, $_POST['username']);

    $full_name = mysqli_real_escape_string($con, $_POST['full_name']);
    $mykad = mysqli_real_escape_string($con, $_POST['mykad']);
    $dob = mysqli_real_escape_string($con, $_POST['dob']);
    $gender = mysqli_real_escape_string($con, $_POST['gender']);
    $phone = mysqli_real_escape_string($con, $_POST['phone']);
    $course_name = mysqli_real_escape_string($con, $_POST['course_name']);
    $semester = mysqli_real_escape_string($con, $_POST['semester']);

    $filename = $_FILES["profile_picture"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["profile_picture"]["size"];
    $allowed_file_types = array('.jpg','.JPG');	

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	    
        $newfilename = $username . $file_ext;
        $pic_path = "uploads/profile/".$newfilename;

        $sql = "UPDATE student 
                SET full_name='$full_name', mykad='$mykad', dob='$dob', gender='$gender', phone='$phone', course_name='$course_name', semester='$semester', profile_pic='$pic_path'
                WHERE email='$email'";

        if (mysqli_query($con, $sql)) {
            move_uploaded_file($_FILES["profile_picture"]["tmp_name"], "../uploads/profile/" . $newfilename);
            echo "<script>alert('Update Successful');window.history.back();</script>";
        } else {
            echo "<script>alert('Error Updating !!!');window.history.back();</script>";
        }        
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
        // unlink($_FILES["note"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }

    
}else if($_POST['type']=='lect'){
    $email = mysqli_real_escape_string($con, $_SESSION['email']);
    $username = mysqli_real_escape_string($con, $_POST['username']);

    $full_name = mysqli_real_escape_string($con, $_POST['full_name']);
    $mykad = mysqli_real_escape_string($con, $_POST['mykad']);
    $dob = mysqli_real_escape_string($con, $_POST['dob']);
    $gender = mysqli_real_escape_string($con, $_POST['gender']);
    $phone = mysqli_real_escape_string($con, $_POST['phone']);

    $filename = $_FILES["profile_picture"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["profile_picture"]["size"];
    $allowed_file_types = array('.jpg','.JPG');	

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	    
        $newfilename = $username . $file_ext;
        $pic_path = "uploads/profile/".$newfilename;

        $sql = "UPDATE lecture 
                SET full_name='$full_name', mykad='$mykad', dob='$dob', gender='$gender', phone='$phone', profile_pic='$pic_path'
                WHERE email='$email'";

        if (mysqli_query($con, $sql)) {
            move_uploaded_file($_FILES["profile_picture"]["tmp_name"], "../uploads/profile/" . $newfilename);
            echo "<script>alert('Update Successful');window.history.back();</script>";
        } else {
            echo "<script>alert('Error Updating !!!');window.history.back();</script>";
        }        
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
        // unlink($_FILES["note"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }
}else if($_POST['type']=='staff'){
    $email = mysqli_real_escape_string($con, $_SESSION['email']);
    $username = mysqli_real_escape_string($con, $_POST['username']);

    $full_name = mysqli_real_escape_string($con, $_POST['full_name']);
    $mykad = mysqli_real_escape_string($con, $_POST['mykad']);
    $dob = mysqli_real_escape_string($con, $_POST['dob']);
    $gender = mysqli_real_escape_string($con, $_POST['gender']);
    $phone = mysqli_real_escape_string($con, $_POST['phone']);
    $department = mysqli_real_escape_string($con, $_POST['department']);

    $filename = $_FILES["profile_picture"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["profile_picture"]["size"];
    $allowed_file_types = array('.jpg','.JPG');	

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	    
        $newfilename = $username . $file_ext;
        $pic_path = "uploads/profile/".$newfilename;

        $sql = "UPDATE staff 
                SET full_name='$full_name', mykad='$mykad', dob='$dob', gender='$gender', phone='$phone', profile_pic='$pic_path', department = '$department'
                WHERE email='$email'";

        if (mysqli_query($con, $sql)) {
            move_uploaded_file($_FILES["profile_picture"]["tmp_name"], "../uploads/profile/" . $newfilename);
            echo "<script>alert('Update Successful');window.history.back();</script>";
        } else {
            echo "<script>alert('Error Updating !!!');window.history.back();</script>";
        }        
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);
        // unlink($_FILES["note"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }
}


// $email = mysqli_real_escape_string($con, $_SESSION['email']);
// $username = mysqli_real_escape_string($con, $_POST['username']);

// $full_name = mysqli_real_escape_string($con, $_POST['full_name']);
// $mykad = mysqli_real_escape_string($con, $_POST['mykad']);
// $dob = mysqli_real_escape_string($con, $_POST['dob']);
// $gender = mysqli_real_escape_string($con, $_POST['gender']);
// $phone = mysqli_real_escape_string($con, $_POST['phone']);
// $course_name = mysqli_real_escape_string($con, $_POST['course_name']);
// $semester = mysqli_real_escape_string($con, $_POST['semester']);

// $sql = "UPDATE student 
//         SET full_name='$full_name', mykad='$mykad', dob='$dob', gender='$gender', phone='$phone', course_name='$course_name', semester='$semester' 
//         WHERE email='$email'";

// if (mysqli_query($con, $sql)) {
//     echo "<script>alert('Update Successful');window.history.back();</script>";
// } else {
//     echo "<script>alert('Error Updating !!!');window.history.back();</script>";
// }

?>
