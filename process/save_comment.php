<?php
session_start();
include("inc_db.php");
include("user_detail.php");
include("setting.php");
include("current_point.php");

$sql = "SELECT * FROM comment_count WHERE comment_email='$_SESSION[email]'";
$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {

    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        $comment_email = mysqli_real_escape_string($con, $_SESSION['email']);
        $start_date = date('Y-m-d H:i:s');
        $end_date = date("Y-m-d H:i:s", strtotime('+24 hours'));
        //start

        $sql5="SELECT count(*) AS point FROM comment WHERE comment_email='$comment_email' AND date_comment>='$start_date' AND date_comment<='$end_date'";

        if($result5=mysqli_query($con,$sql5)){
            // Fetch one and one row
            while ($row5=mysqli_fetch_array($result5)){
                    $comment_count = $row5['point'];

                    if($comment_count>=3){
                        $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
                        $comment = mysqli_real_escape_string($con, $_POST['comment']);

                        $new = $row['count_comment'] + 1;
                            
                        $sql1 = "UPDATE comment_count SET count_comment='$new' WHERE comment_email='$comment_email' AND forum_id='$forum_id'";

                        $sql2="INSERT INTO comment (comment_email, forum_id, comment, full_name)
                                VALUES ('$comment_email', '$forum_id', '$comment', '$full_name')";

                        if (!mysqli_query($con,$sql1)) {
                                die('Error: ' . mysqli_error($con));
                            }

                        if (!mysqli_query($con,$sql2)) {
                                die('Error: ' . mysqli_error($con));
                            }

                        echo "<script>alert('Comment Submited.');document.location.href='../student/singleForum?id=".$forum_id."';</script>"; 
                    }else{
                        $sql6="SELECT * FROM comment_point WHERE commentp_email='$comment_email'";

                        if ($result6=mysqli_query($con,$sql6)){
                            // Fetch one and one row
                            while ($row6=mysqli_fetch_array($result6)){
                                    $current_point1 = $row6['current_point'];
                                }
                            // Free result set
                            mysqli_free_result($result);
                        }

                        $new1 = $current_point1 + $comment_point;
                        $new2 = $current_point + $comment_point;

                        $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
                        $comment = mysqli_real_escape_string($con, $_POST['comment']);
    
                        $new = $row['count_comment'] + 1;
                            
                        $sql1 = "UPDATE comment_count SET count_comment='$new' WHERE comment_email='$comment_email' AND forum_id='$forum_id'";

                        $sql3 = "UPDATE comment_point SET current_point='$new1' WHERE commentp_email='$comment_email'";

                        $sql4 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$comment_email'";
    
                        $sql2="INSERT INTO comment (comment_email, forum_id, comment, full_name)
                                VALUES ('$comment_email', '$forum_id', '$comment', '$full_name')";
    
                        if (!mysqli_query($con,$sql1)) {
                            die('Error: ' . mysqli_error($con));
                        }
    
                        if (!mysqli_query($con,$sql2)) {
                            die('Error: ' . mysqli_error($con));
                        }

                        if (!mysqli_query($con,$sql3)) {
                            die('Error: ' . mysqli_error($con));
                        }

                        if (!mysqli_query($con,$sql4)) {
                            die('Error: ' . mysqli_error($con));
                        }
    
                        echo "<script>alert('Comment Submited.');document.location.href='../student/singleForum?id=".$forum_id."';</script>"; 
                    }                  
                }
            // Free result set
            mysqli_free_result($result);
        }
        else{
            echo("Error description: " . mysqli_error($con));
        }

        // end
        // $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
        // $comment = mysqli_real_escape_string($con, $_POST['comment']);

        // $new = $row['count_comment'] + 1;
            
        // $sql1 = "UPDATE comment_count SET count_comment='$new' WHERE comment_email='$comment_email' AND forum_id='$forum_id'";

        // $sql2="INSERT INTO comment (comment_email, forum_id, comment)
        //         VALUES ('$comment_email', '$forum_id', '$comment')";

        // if (!mysqli_query($con,$sql1)) {
        //         die('Error: ' . mysqli_error($con));
        //     }

        // if (!mysqli_query($con,$sql2)) {
        //         die('Error: ' . mysqli_error($con));
        //     }

        // echo "<script>alert('Comment Submited.');document.location.href='../student/singleForum?id=".$forum_id."';</script>"; 
    }
} else {

    $comment_email = mysqli_real_escape_string($con, $_SESSION['email']);
    $forum_id = mysqli_real_escape_string($con, $_POST['forum_id']);
    $comment = mysqli_real_escape_string($con, $_POST['comment']);

    $new2 = $current_point + $comment_point;

    $sql1="INSERT INTO comment_count (comment_email, forum_id, count_comment)
            VALUES ('$comment_email', '$forum_id', '1')";

    $sql2="INSERT INTO comment (comment_email, forum_id, comment, full_name)
            VALUES ('$comment_email', '$forum_id', '$comment', '$full_name')";

    $sql3 = "UPDATE current_point SET current_point='$new2' WHERE point_email='$comment_email'";

    $sql4 = "UPDATE comment_point SET current_point='$new2' WHERE commentp_email='$comment_email'";

    if (!mysqli_query($con,$sql1)) {
        die('Error: ' . mysqli_error($con));
    }

    if (!mysqli_query($con,$sql2)) {
        die('Error: ' . mysqli_error($con));
    }

    if (!mysqli_query($con,$sql3)) {
        die('Error: ' . mysqli_error($con));
    }

    if (!mysqli_query($con,$sql4)) {
        die('Error: ' . mysqli_error($con));
    }

    echo "<script>alert('Comment Submited.');document.location.href='../student/singleForum?id=".$forum_id."';</script>"; 
}

mysqli_close($con);
?>