<?php
session_start();
include("inc_db.php");

// Upload and Rename File
if (isset($_POST['submit']))
{
	$forms_title = mysqli_real_escape_string($con, $_POST['forms_title']);
    $forms_category = mysqli_real_escape_string($con, $_POST['forms_category']);
    
    $filename = $_FILES["forms"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["forms"]["size"];
    $allowed_file_types = array('.doc','.docx','.pdf', '.xls', '.xlsx', '.ppt', '.pptx');	

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	
        // Rename file
        $newfilename = $forms_title . $file_ext;
        if (file_exists("../uploads/forms/" . $newfilename))
        {
            // file already exists error
            echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
        }
        else
        {		
            $forms_path = "uploads/forms/".$newfilename;

            $sql="INSERT INTO forms (owner_email, forms_title, forms_path, type, forms_category)
                    VALUES ('$_SESSION[email]', '$forms_title', '$forms_path', '$file_ext', '$forms_category')";
                    
            if (!mysqli_query($con,$sql)) {
                die('Error: ' . mysqli_error($con));
            }else {
                move_uploaded_file($_FILES["forms"]["tmp_name"], "../uploads/forms/" . $newfilename);
                echo "<script>alert('Forms Submited.');document.location.href='../admin/formsDownload';</script>"; 
            }
        }
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);			
        // unlink($_FILES["forms"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }
	
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>