<?php
include("inc_db.php");

$sql="SELECT * FROM current_point,user,lecture
        WHERE user.email=lecture.email
        AND current_point.point_email=user.email
        AND user.role='Lecture' 
        ORDER BY current_point 
        DESC LIMIT 10";

if ($result=mysqli_query($con,$sql)){
    // Fetch one and one row
    while ($row=mysqli_fetch_array($result)){

?>
                    <div class="card single-artist mb-xl-2 mb-lg-3">
                        <div class="card-body">
                            <div class="media">
                                <div class="user-img mr-4">
                                    <?php
                                        if($row['profile_pic']==NULL){
                                    ?>
                                            <img src="../assets/images/avatar/50.png" height="50" width="50" alt="">
                                    <?php
                                        }else{
                                    ?>
                                            <img src="../<?php echo $row['profile_pic'] ?>" height="50" width="50" alt="">
                                    <?php
                                        }
                                    ?>
                                </div>
                                <div class="media-body">
                                    <h5><?php echo $row['full_name']; ?></h5>
                                    <p><?php include("rank.php"); ?><br/><b><?php echo $row['current_point']; ?></b> Points</p>
                                </div>
                            </div>
                        </div>
                    </div>
<?php

        }
    // Free result set
    mysqli_free_result($result);
}

?>