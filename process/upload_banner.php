<?php
session_start();
include("inc_db.php");

// Upload and Rename File
if (isset($_POST['submit']))
{
	$banner_title = mysqli_real_escape_string($con, $_POST['banner_title']);
    
    $filename = $_FILES["banner"]["name"];
    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
    $file_ext = substr($filename, strripos($filename, '.')); // get file name
    $filesize = $_FILES["banner"]["size"];
    $allowed_file_types = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG');

    if (in_array($file_ext,$allowed_file_types) && ($filesize < 500000000))
    {	
        // Rename file
        $newfilename = $banner_title . $file_ext;
        if (file_exists("../uploads/banner/" . $newfilename))
        {
            // file already exists error
            echo "<script>alert('File Already Exists !!!');window.history.back();</script>";  
        }
        else
        {		
            $banner_path = "uploads/banner/".$newfilename;

            $sql="INSERT INTO banner (banner_title, banner_path, type)
                    VALUES ('$banner_title', '$banner_path', '$file_ext')";
                    
            if (!mysqli_query($con,$sql)) {
                die('Error: ' . mysqli_error($con));
            }else {
                move_uploaded_file($_FILES["banner"]["tmp_name"], "../uploads/banner/" . $newfilename);
                echo "<script>alert('Banner Submited.');document.location.href='../admin/viewBanner';</script>"; 
            }
        }
    }
    elseif (empty($file_basename))
    {	
        // file selection error
        echo "<script>alert('No File Submited !!!');window.history.back();</script>";  
    } 
    elseif ($filesize > 500000000)
    {	
        // file size error
        echo "<script>alert('File Size Was Big !!!');window.history.back();</script>";  
    }
    else
    {
        // file type error
        // echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);			
        // unlink($_FILES["banner"]["tmp_name"]);
        echo "<script>alert('Upload File Not Supported !!!');window.history.back();</script>";
    }
	
}else{
	echo "<script>alert('Use Submit Button !!!');window.history.back();</script>";  
}

mysqli_close($con);

?>