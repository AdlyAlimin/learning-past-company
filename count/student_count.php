<?php
include("../process/inc_db.php");

$sql="SELECT COUNT(*) as active FROM user WHERE role='Student' AND status='Active'";
$result=mysqli_query($con,$sql);

// Associative array
$row=mysqli_fetch_assoc($result);

$active = $row['active'];

// Free result set
mysqli_free_result($result);

$sql="SELECT COUNT(*) as banned FROM user WHERE role='Student' AND status='Banned'";
$result=mysqli_query($con,$sql);

// Associative array
$row=mysqli_fetch_assoc($result);

$banned = $row['banned'];

// Free result set
mysqli_free_result($result);

$sql="SELECT COUNT(*) as inactive FROM user WHERE role='Student' AND status='Inactive'";
$result=mysqli_query($con,$sql);

// Associative array
$row=mysqli_fetch_assoc($result);

$inactive = $row['inactive'];

// Free result set
mysqli_free_result($result);

?>